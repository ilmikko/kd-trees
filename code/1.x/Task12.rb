require_relative '../OneDReader.rb';

class Task12
    def insert(*values)
        @list += values;
        sort;
    end

    def query(start, finish)
        start_index = binarysearch(@list, start, 0, @list.length);

        reported = [];

        (start_index...@list.length).each{|i|
            # Check the last node that is not in range and clone the list
            if @list[i] > finish
                reported = @list[start_index...i];
                break;
            end
        }

        return reported;
    end

    private

    def initialize
        @list=[];
    end

    def sort
        @list=mergesort(@list);
    end

    def mergesort(list)
        return list if list.length==1;

        # Find the middle point
        middle = list.length / 2;

        # Partition the big list into smaller halves
        left = list[0...middle];
        right = list[middle..-1];

        # Sort the smaller lists
        left = mergesort(left);
        right = mergesort(right);

        return merge(left, right);
    end

    def merge(left,right)
        merged = [];

        # Merge the values from two lists
        while !left.empty? and !right.empty?
            if left.first <= right.first
                merged << left.shift;
            else
                merged << right.shift;
            end
        end
        
        # Merge the rest of either list that isn't empty
        if !left.empty?
            merged += left;
        else
            merged += right;
        end

        return merged;
    end

    def binarysearch(list, value, lo, hi)
        if hi-lo > 1
            middle = lo+(hi-lo)/2;
            
            if list[middle]==value
                return middle
            elsif list[middle]>value
                return binarysearch(list, value, lo, middle);
            elsif value>list[middle]
                return binarysearch(list, value, middle, hi);
            end
        else
            # If step is too small, which means the value doesn't exist,
            # return the value to the right (larger) of the intended value
            return hi;
        end
    end
end