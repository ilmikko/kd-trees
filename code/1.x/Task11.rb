require_relative '../OneDReader.rb';

class LinkedListNode
    attr_accessor :next, :value;

    def each
        node = self;
        while node != nil
            yield node.value;
            node = node.next;
        end
    end

    private

    def initialize(value, pointer=nil)
        @value=value;
        @next=pointer;
    end
end

class Task11
    def insert(*values)
        values.each{ |value|
            @list = LinkedListNode.new(value,@list);
        }
    end
    def query(start,finish)
        return [] if @list.nil?;

        reported = [];

        @list.each{ |val|
            # Report if node is in range
            reported << val if start <= val and val <= finish;
        }

        return reported;
    end
end