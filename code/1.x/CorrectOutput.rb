class CorrectOutput
    def insert(*values)
        @list += values;
    end
    def query(start,finish)
        reported = [];

        @list.each{|item|
            reported << item if (start <= item and item <= finish);
        }
        
        return reported;
    end

    private

    def initialize
        @list = [];
    end
end