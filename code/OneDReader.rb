#
# Reader for a one dimensional range query (format described in task 2.1.).
#

class OneDReader
    def read
        # Reading input
        elements, queries = @input.gets.split.map{|s|s.to_i};

        # Store each line of integers in our data structure
        (0...elements).each{ |i| @structure.insert(@input.gets.to_i); }

        # Run the queries
        (0...queries).each{ |i|
            # Get start and finish values
            start, finish = @input.gets.split.map{|s|s.to_i};

            # Output the result of the query
            @output.puts(@structure.query(start,finish).join(' '));
        }
    end

    private

    def initialize(structure, input: $stdin, output: $stdout)
        # Structure has (at least) two methods: insert(number) and query(start, finish)
        @structure = structure;

        # Redirection of streams for convenience
        @input = input;
        @output = output;
    end
end