require_relative 'Task55.rb';

def vector_subtract(a, b)
    [a[0]-b[0],a[1]-b[1]];
end

def cross_product(a, b)
    a[0]*b[1]-a[1]*b[0];
end

def get_side(a, b)
    x = cross_product(a, b);
    if x < 0
        return :left;
    elsif x > 0
        return :right;
    else
        return :none;
    end
end

def PointInConvex(convexshape, point)
    len = convexshape.size;

    previous_side = nil;

    (0...len).each{ |i|
        a = convexshape[i];
        b = convexshape[(i+1)%len];

        affine_segment = vector_subtract(b, a);
        affine_point = vector_subtract(point, a);

        current_side = get_side(affine_segment, affine_point);
        if current_side == :none
            next;
        elsif previous_side.nil?
            previous_side = current_side;
        elsif previous_side != current_side
            return false;
        end
    };

    return true;
end

class Node
    def is_in? convexshape
        PointInConvex(convexshape, @value);
    end
end

class ConvexShape
    attr_reader :bounds, :points;

    def [](v)
        @points[v];
    end

    def size
        @points.size;
    end

    def to_s
        "#{@points}";
    end
    
    private

    def get_bounds
        # Generate a bounding box from the points

        min1, max1 = [Float::INFINITY,-Float::INFINITY];
        min2, max2 = [Float::INFINITY,-Float::INFINITY];
        
        @points.each{ |point|
            min1 = point[0] if point[0] < min1;
            max1 = point[0] if point[0] > max1;
            min2 = point[1] if point[1] < min2;
            max2 = point[1] if point[1] > max2;
        };

        [[min1,max1],[min2,max2]];
    end

    def initialize(*points)
        @points = points;
        @bounds = get_bounds;
    end
end

# Get the equation for a given start and end point
def get_eq(s, e)
    x1, y1 = s;
    x2, y2 = e;

    a = y2-y1;
    b = x1-x2;
    c = a*x1+b*y1;

    return [a,b,c];
end

# Check whether a number is between two other numbers
def between(min, v, max)
    # Ensure that min <= max
    min, max = max, min if min > max;
    return (min <= v and v <= max);
end

# Check whether two lines intersect
def line_intersect(line1, line2)
    s1, e1 = line1;
    s2, e2 = line2;

    a1, b1, c1 = get_eq(s1, e1);
    a2, b2, c2 = get_eq(s2, e2);

    det = a1*b2 - a2*b1;
    if (det==0)
        # Parallel
        return false;
    else
        # Where do they intersect?
        x = (b2*c1-b1*c2).to_f/det;
        y = (a1*c2-a2*c1).to_f/det;

        a = between(s1[0], x, e1[0]);
        b = between(s1[1], y, e1[1]);
        c = between(s2[0], x, e2[0]);
        d = between(s2[1], y, e2[1]);

        # Is intersection point outside of either of our equations? If so, return false
        return false if !(a and b and c and d);
    end

    return true;
end

def IsFullyContainedWithin(node, convexshape)
    region = GetRegion(node);
    
    # Check if the region is unbound; we can automatically say false without extra steps.
    return false if region.any? { |min, max| min.infinite? or max.infinite? };
    
    corners = GetCorners(region);

    # HACK: Task 5.6 works only in 2D space, so we know there will be four corners.
    # The order the corners are generated does not imply a good ordering for a rectangle, when looped through the edges are in a wrong order.
    # E.g. for a rectangle ABCD edges would be AB, BC, CD, DA instead of AB, BD, DC, CA. We can remedy this by swapping the two first corners around.
    # This makes ABCD -> BACD, and the edges will be: BA, AC, CD, DB.
    corners[0], corners[1] = corners[1], corners[0];

    # 1. Check if corners are inside the convex shape
    return false if corners.any? { |corner| !PointInConvex(convexshape, corner); };

    # 2. Check if any line of the polygon intersects the region edges
    points = convexshape.points;
    points.size.times{ |i|
        a = points[i];
        b = points[(i+1)%points.size];

        corners.size.times{ |h|
            c = corners[h];
            d = corners[(h+1)%corners.size];

            return false if line_intersect([a,b],[c,d]);
        };
    };

    return true;
end

def SearchKDTree(node, convexshape)

    # If node is a leaf, return if the leaf matches the query.
    if node.leaf?
        if node.is_in? convexshape
            return [node];
        else
            return [];
        end
    end

    mins = [-Float::INFINITY]*node.value.count;
    maxes = [Float::INFINITY]*node.value.count;
    
    reported_nodes = [];
    
    # Use the bounding box of the convex to determine region intersections.
    
    # TODO: bounding box is suboptimal.
    # TODO: is it? We'd still be crossing the same regions.
    
    if !node.left.nil?

        c = maxes.dup;

        c[node.axis] = node.value[node.axis]; # bound by axis

        a = convexshape.bounds;
        b = mins.zip(c);

        if IsFullyContainedWithin(node.left, convexshape)
            reported_nodes += node.left.subtree;
        elsif intersect(a,b) then
            reported_nodes += SearchKDTree(node.left, convexshape);
        end
    end

    if !node.right.nil?

        c = mins.dup;

        c[node.axis] = node.value[node.axis]; # bound by axis

        a = convexshape.bounds;
        b = c.zip(maxes);

        if IsFullyContainedWithin(node.right, convexshape)
            reported_nodes += node.right.subtree;
        elsif intersect(a,b) then
            reported_nodes += SearchKDTree(node.right, convexshape);
        end
    end

    return reported_nodes;
end

class Task56
    def insert(*values)
        @list += values;
    end

    def query(*points)
        prep_tree if !ready?;
        SearchKDTree(@tree, ConvexShape.new(*points));
    end

    private

    def ready?
        !@tree.nil?;
    end

    def prep_tree
        @tree = BuildKDTree(@list);
    end

    def initialize
        @list = [];
    end
end