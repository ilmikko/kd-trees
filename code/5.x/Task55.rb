require_relative 'Task54.rb';

class Node
    def is_in? position, radius
        n_dist(self.value, position) <= radius
    end
end

# A distance in n dimensions between two points
def n_dist(point1, point2)
    raise "Dimension mismatch in distance matching (#{point1.count}!=#{point2.count})" if point1.count != point2.count;
    sum = 0;
    point1.count.times{ |i|
        d = point1[i]-point2[i]; 
        sum += d*d;
    }
    return Math.sqrt(sum);
end

def intersect_left(node, position, radius)
    # does the left region of this node intersect the sphere?
    axis = node.axis;
    val = node.value[axis];

    if position[axis] <= val
        # Sphere is on the left side, so it does intersect the left region
        return true;
    else
        # Sphere is on the right side, check if it's intersecting the left region through radius
        return (position[axis]-radius <= val);
    end
end

def intersect_right(node, position, radius)
    # does the right region of this node intersect the sphere?
    axis = node.axis;
    val = node.value[axis];

    if position[axis] >= val
        # Sphere is on the right side, so it does intersect the right region
        return true;
    else
        # Sphere is on the left side, check if it's intersecting the right region through radius
        return (position[axis]+radius >= val);
    end
end

# Get a region for a node
def GetRegion(node, dimensionality:nil)
    # This method gets the 2^d corners of a region, using magic
    dimensionality = node.value.count if dimensionality.nil?;

    axis_corner_min = [-Float::INFINITY]*dimensionality;
    axis_corner_max = [Float::INFINITY]*dimensionality;

    focusNode = node;
    dimensionality.times{ |i| 
        # Get parent as we don't care about the node
        focusNode = focusNode.parent;

        break if focusNode.nil?; # we have reached root

        axisParentNode = GetAxisParentNode(focusNode);
        
        # Current axis we are working on
        axis = focusNode.axis;

        # puts("Current dimension: #{axis}");

        a = focusNode.value[axis];
        val = node.value[axis];

        if axisParentNode.nil?
            if val <= a
                # left region
                axis_corner_max[axis] = a;
            else
                # right region
                axis_corner_min[axis] = a;
            end
        else
            b = axisParentNode.value[axis];

            a, b = b, a if a > b; # Ensure that a comes before b (a <= b)

            # Determine if the node is bound by both a and b, or only one of them

            if a < val and val <= b
                # Bound by both
                # puts("Node #{val} is bound by both #{a} and #{b} on axis #{axis}");
                axis_corner_min[axis] = a;
                axis_corner_max[axis] = b;
            elsif val <= a and val <= b
                # On the left side - the closest (minimum) of a,b is max
                # puts("Node #{val} is bound by only #{[a,b].min} on axis #{axis}");
                axis_corner_max[axis] = [a,b].min;
            elsif val > a and val > b
                # On the right side - the closest (maximum) of a,b is min
                # puts("Node #{val} is bound by only #{[a,b].max} on axis #{axis}");
                axis_corner_min[axis] = [a,b].max;
            else
                raise "I am not sure if these are exclusive";
            end
        end
    };

    return axis_corner_min.zip(axis_corner_max); # Convert to min_max format
end

def GetCorners(min_max)
    dimensionality = min_max.size;

    corners = [];

    # An element in n dimensions will have 2**n corners.
    (2**dimensionality).times{ |i|
        corner = [];
        dimensionality.times{ |h|
            # Make sure to pick every combination of these coordinates
            corner << min_max[h][i/(2**h)%2];
        };
        corners << corner;
    };

    return corners;
end

def IsFullyContainedWithin(node, position, radius)
    # Get the region for this node, and calculate the corners from there
    region = GetRegion(node);
    
    # Check if the region is unbound; we can automatically say false without extra steps.
    return false if region.any? {|min, max| min.infinite? or max.infinite? };

    # Calculate the corners. We already have two. (min and max).
    # The way we do it is calculate all combinations 
    corners = GetCorners(region);

    # Find the maximum distance to a corner.
    max_dist = corners.reduce(0){ |max, corner|
        dist = n_dist(position, corner);
        dist > max ? dist : max;
    };

    # puts("Maximum distance to a corner: #{max_dist}");

    # If max_dist <= radius, the region is fully contained.
    # puts("BOO") if max_dist <= radius;
    return max_dist <= radius;
end

def SearchKDTree(node, position, radius)
    if node.leaf?
        # check if point stored in v lies in R, return if so.
        if node.is_in? position, radius
            return [node];
        else
            return [];
        end
    end

    mins = [-Float::INFINITY]*node.value.count;
    maxes = [Float::INFINITY]*node.value.count;

    zeroes = [0]*node.value.count;

    reported_nodes = [];

    if !node.left.nil?
        # puts("Node #{node} lt test: #{a} vs #{b} -> #{intersect(a,b)}");

        if IsFullyContainedWithin(node.left, position, radius) then
            reported_nodes += node.left.subtree;
        # else if the region of the left child of v intersects R then
        elsif intersect_left(node, position, radius) then
            reported_nodes += SearchKDTree(node.left, position, radius);
        end
    end

    if !node.right.nil?
        # puts("Node #{node} rt test: #{a} vs #{b} -> #{intersect(a,b)}");
        
        if IsFullyContainedWithin(node.right, position, radius) then
            reported_nodes += node.right.subtree;
        # else if the region of the right child of v intersects R then
        elsif intersect_right(node, position, radius) then
            reported_nodes += SearchKDTree(node.right, position, radius);
        end
    end
    
    return reported_nodes;
end

class Task55
    def insert(*values)
        @list += values;
    end

    def query(position, radius)
        prep_tree if !ready?;
        SearchKDTree(@tree, position, radius);
    end

    private

    def ready?
        !@tree.nil?;
    end

    def prep_tree
        @tree = BuildKDTree(@list);
    end

    def initialize
        @list = [];
    end
end