require_relative 'Task55';
require_relative '../MultiDReader';

class MultiDReader
    def read_query
        # Get start and finish values
        position, radius = gets.split(/(?<=\])\s/);
        radius = radius.to_i; # make it an int

        position = position.gsub(/(\[|\])/,'').split.map{ |s| s.to_i }; # remove [] and split

        # Output the result of the query
        @structure.query(position,radius).map{ |a| "[#{a.value.join(' ')}]"; }.join(' ');
    end
end

MultiDReader.new(Task55.new).read;