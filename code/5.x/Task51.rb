require_relative 'MultiDReader';

def median(list, k=nil, axis: 0)
    # Cannot find a median off of an empty list
    return nil if list.empty?;

    # If the list passed is an argument of form [1,2,3,4], redo with the zipped arguments.
    return median(list.zip, k, axis: axis) if !list.first.is_a? Array;

    len = list.length;

    # len-1 for rounding the uneven case down
    k = (len-1)/2 if k.nil?;

    if len <= 5
        return list.sort{ |a,b| a[axis]-b[axis]; }[k];
    else
        # Partition the list into subsets of 5.
        partitions = [];

        # Handle the even 5 length partitions
        for i in 0...len/5
            partitions << list[i*5...(i+1)*5];
        end

        # Handle the remaining partition if the divide wasn't even (the last one might contain 1-4 elements in this case)
        partitions << list[-(len%5)..-1] if len%5>0;

        # Select a median for each partition
        medians = partitions.map{|partition| median(partition,partition.length/2);};

        #puts("#{partitions} and #{medians}");

        # Get our friend master median m
        m = median(medians, len/10);

        # Partition the list into three groups depending on the relation to m
        less = [];
        equal = [];
        more = [];

        list.each{|item|
            if item[axis] < m[axis]
                less << item;
            elsif item[axis] > m[axis]
                more << item;
            else
                equal << item;
            end
        }

        if k < less.count
            # k <  less.count
            return median(less, k);
        elsif k > less.count
            # k >  less.count
            return median(more, k-less.count-1);
        else
            # k == less.count
            return m;
        end
    end
end

def BuildKDTree(points, depth=0, dimensionality: nil)
    return nil if points.empty?;

    # Get the dimensionality from the first point
    dimensionality = points.first.length if dimensionality.nil?;

    axis = depth % dimensionality;
    
    # if P contains only one point
    return Node.new(points.first, axis, leaf:true) if points.count==1;

    median = median(points, axis: axis);

    # Allocate the points in the tree to left / right side of the median
    left = [];
    right = [];
    points.each{|point|
        if point[axis] <= median[axis]
            left << point;
        elsif point[axis] > median[axis]
            right << point;
        end
    }

    # Build the node representation
    node = Node.new(median, axis);

    if !left.empty?
        node.left = BuildKDTree(left, depth+1);
        node.left.parent = node;
    end
    if !right.empty?
        node.right = BuildKDTree(right, depth+1);
        node.right.parent = node;
    end

    return node;
end

class Node
    attr_accessor :left, :right, :parent;
    attr_reader :value, :axis;

    def leaf?
        !!@leaf;
    end

    def is_in? min_max
        raise "Dimension mismatch in range query (#{min_max.count}!=#{@value.count})" if min_max.count!=@value.count;

        #puts("Node #{self} is in? #{min_max}");

        (0...@value.count).each{|dimension|
            min, max = min_max[dimension];
            #puts("#{@value[dimension]} < #{min} or #{@value[dimension]} > #{max}");
            return false if @value[dimension] < min or @value[dimension] > max;
        }

        return true;
    end

    def inspect
        to_s;
    end

    def to_s
        "<#{@value} (#{@axis})>";
    end

    def subtree
        st = [];

        st += @left.subtree if !@left.nil?;
        st << @value if leaf?;
        st += @right.subtree if !@right.nil?;

        return st;
    end

    private

    def initialize(value, axis=0, leaf:false, parent:nil)
        @value=value;
        @axis=axis;
        @leaf=leaf;

        @parent=parent;

        @left=nil;
        @right=nil;
    end
end

def intersect(min_max1, min_max2)
    raise "Dimension mismatch in intersection query (#{min_max1.count}!=#{min_max2.count})" if min_max1.count!=min_max2.count;

    # puts("min_max1: #{min_max1}");
    # puts("min_max2: #{min_max2}");

    (0...min_max1.count).each{|dimension|
        min1,max1 = min_max1[dimension];
        min2,max2 = min_max2[dimension];

        # puts("min1, max1: #{min1} #{max1}");
        # puts("min2, max2: #{min2} #{max2}");

        c1 = (min2 <= min1 and min1 <= max2);
        c2 = (min2 <= max1 and max1 <= max2);
        c3 = (min1 <= min2 and min2 <= max1);
        c4 = (min1 <= max2 and max2 <= max1);

        # puts("#{c1} #{c2} #{c3} #{c4}");

        return false if !(c1 or c2 or c3 or c4);
    }
    
    # puts("intersect");
    return true;
end

# This part is implemented in 5.2
def IsFullyContainedWithin(node, range)
    false;
end

# Searching a KD-tree
def SearchKDTree(node, min_max, dimensionality:nil)
    # Get the dimensionality from min_max.
    dimensionality = min_max.size if dimensionality.nil?;

    # If node is a leaf, report the leaf.
    if node.leaf?
        if node.is_in? min_max
            return [node.value];
        else
            return [];
        end
    end

    # Initialize the bounds
    mins = [-Float::INFINITY]*dimensionality;
    maxes = [Float::INFINITY]*dimensionality;
    
    reported_nodes = [];

    if !node.left.nil?
        c = maxes.dup;

        c[node.axis] = node.value[node.axis]; # bound by axis

        a = min_max;
        b = mins.zip(c);

        if IsFullyContainedWithin(node.left, min_max) then
            reported_nodes += node.left.subtree;
        # else if the region of the left child of v intersects R then
        elsif intersect(a,b) then
            reported_nodes += SearchKDTree(node.left, min_max);
        end
    end

    if !node.right.nil?
        c = mins.dup;

        c[node.axis] = node.value[node.axis]; # bound by axis

        a = min_max;
        b = c.zip(maxes);
        
        if IsFullyContainedWithin(node.right, min_max) then
            reported_nodes += node.right.subtree;
        # else if the region of the right child of v intersects R then
        elsif intersect(a,b) then
            reported_nodes += SearchKDTree(node.right, min_max);
        end
    end

    return reported_nodes;
end

class Task51
    def insert(*values)
        @list += values;
    end

    def query(min_max)
        prep_tree if !ready?;
        SearchKDTree(@tree, min_max);
    end

    private

    def ready?
        !@tree.nil?;
    end

    def prep_tree
        @tree = BuildKDTree(@list);
    end

    def initialize
        @list = [];
    end
end

# MultiDReader.new(Task51.new).read;