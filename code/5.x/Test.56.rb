require_relative 'Task56';
require_relative '../MultiDReader';

def test(listofthings, convexshape)
    testing = {};
    real_results = listofthings.select{ |elem|
        PointInConvex(convexshape, elem);
    };

    real_results.each{ |result|
        testing[result] = 1;
    }

    # Composite numbers
    listofthings = listofthings.map{ |point| pointToComposite(point); };

    tree = BuildKDTree(listofthings);

    results = SearchKDTree(tree,convexshape).map{|node| node.value};

    results = results.map{ |point| compositeToPoint(point); };

    results.each{ |result|
        if testing[result].nil?
            puts("ERR #{real_results} vs #{results}");
            puts("IN #{listofthings} -> #{convexshape}");
            puts("#{testing}");
            puts("tried to insert #{result}");
            raise "lol u suck";
        end

        testing[result] += 1;
    }

    if testing.any? { |k,v| v!=2 }
        puts("ERR #{real_results} vs #{results}");
        puts("IN #{listofthings} -> #{convexshape}");
        puts("#{testing}");
        raise "lol u suck much!" ;
    end
    puts("OK #{results} vs #{convexshape}");
end

def gen_list(max=10)
    arrs = {};
    while arrs.size < max
        arrs[[rand(max),rand(max)]] = 1;
    end
    return arrs.keys;
end

def gen_convex(max=10)
    size = 3+rand(8);

    sect = (2*Math::PI) / size;

    arrs = [];

    size.times{ |i|
        x = Math.sin(i*sect)*max+((rand-0.5)*5);
        y = Math.cos(i*sect)*max+((rand-0.5)*5);

        arrs << [x, y];
    };

    return ConvexShape.new *arrs;
end

while true
    size = 14;
    listofthings = gen_list size;
    convexshape = gen_convex size;

    # Test case 1
    # listofthings = [[4, 3], [0, 10], [8, 8], [9, 0], [3, 4], [1, 1], [5, 2], [2, 9], [7, 6], [10, 5], [6, 7]];

    # listofthings = [[0, 0], [2, 2], [4, 4], [6, 6], [8, 8], [10, 10], [12, 12], [14, 14], [16, 16], [18, 18], [20, 20]];
    # convexshape = ConvexShape.new [8.42,16.93],[23.87,33.34],[54.76,5.74],[19.36,-13.64];

    # tree = BuildKDTree(listofthings);

    # node = tree.right.left.left.right;
    # region = GetRegion(node);
    # puts("Node #{node} region is #{region}");

    # puts(IsFullyContainedWithin(node, convexshape));

    # sleep 100;

    test(listofthings, convexshape);
end