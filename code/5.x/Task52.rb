require_relative 'Task51.rb';

def GetAxisParentNode(node)
    parentNode = node.parent;

    while !parentNode.nil? and (parentNode.axis != node.axis)
        parentNode = parentNode.parent;
    end

    return parentNode;
end

def IsFullyContainedWithin(node, min_max, dimensionality: nil)
    focus_node = node;

    # Get dimensionality from min_max if it is undefined
    dimensionality = min_max.count if dimensionality.nil?;

    min_max2 = [];

    dimensionality.times{ |d|
        # Move onto the next parent node
        focus_node = focus_node.parent;

        # We have reached the root node, which regions cannot be fully contained by a finite range query
        return false if focus_node.nil?;

        # Get the axis we are working on
        axis = focus_node.axis;

        # Check whether the focus_node and focus_node's axis parent contain this node
        axis_parent_node = GetAxisParentNode(node);

        # No axis parent node means we are not contained on one axis
        return false if axis_parent_node.nil?;

        min, max = focus_node.value[axis], axis_parent_node.value[axis];

        # puts("focus: #{focus_node}, axp: #{axis_parent_node}");

        # Ensure min <= max
        min, max = max, min if min > max;
        val = node.value[axis];

        # Check if the node containment is true
        return false if !(min < val and val < max);

        min_max2 << [min,max];
    };

    # Check if the containment actually is in our range
    # true containment: for each dimension d, min1 < min2 < max1 and min1 < max2 < max1
    # puts("#{min_max} vs #{min_max2}");

    dimensionality.times{ |d| 
        min1, max1 = min_max[d];
        min2, max2 = min_max2[d];

        return false if !((min1 <= min2 and min2 <= max1) and (min1 <= max2 and max2 <= max1));
    };


    return true;
end