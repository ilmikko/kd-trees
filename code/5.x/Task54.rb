require_relative 'Task52.rb';

class CompositeNumber
    include Comparable;

    attr_reader :value;
    
    def to_s
        "(#{@value.join('|')})";
    end

    def to_i
        @value.first.to_i;
    end

    def to_f
        @value.first.to_f;
    end

    def inspect
        to_s;
    end
    
    def infinite?
        @value.first.infinite?;
    end

    def coerce(other)
        if other.is_a? Numeric
            [other, @value.first.to_i];
        end
    end

    def -(other)
        if other.is_a? Numeric
            return @value.first-other;
        elsif other.is_a? CompositeNumber
            # Coerce for sorting
            @value.count.times{ |i|
                d = @value[i]-other.value[i];
                return d if d != 0;
            }
            return 0;
        end
    end

    def <=>(other)
        if other.is_a? Numeric
            a = @value.first;
            b = other;
    
            # This shouldn't happen in our tasks
            return 0 if a == b;
    
            if a < b
                return -1;
            elsif a > b
                return 1;
            end
        elsif other.is_a? CompositeNumber
            @value.length.times{ |i|
                a = @value[i];
                b = other.value[i];
                
                # a < a' or (a = a' and b < b')
                next if a == b;
                
                if a < b
                    return -1;
                elsif a > b
                    return 1;
                end
            }
        else
            raise "Cannot compare classes #{self.class} and #{other.class}";
        end
        
        # Truly equal
        return 0;
    end

    private

    def initialize(*value)
        @value = value;
    end
end

def pointToComposite(point)
    # Convert each coordinate number to a composite number
    point.map{ |n|
        p = point.dup;
        p.delete_at(p.index(n));
        CompositeNumber.new(n,*p);
    };
end

def compositeToPoint(point)
    # Convert each composite number to a coordinate number
    point.map{ |n|
        n.to_i
    }
end