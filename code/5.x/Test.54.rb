require_relative 'Task54.rb';

def test(listofthings, range)
    testing = {};
    real_results = listofthings.select{ |elem|
        yes = true;
        (0...elem.length).each{ |i|
            yes = false if !(range[i].first <= elem[i] and elem[i] <= range[i].last);
        }
        yes;
    };

    real_results.each{ |result|
        testing[result] = 1;
    }

    # Map the point numbers to composite numbers
    listofthings = listofthings.map{ |point| pointToComposite(point); };

    tree = BuildKDTree(listofthings);

    results = SearchKDTree(tree,range).map{|node| node.value};

    # Map the composite numbers back to point numbers
    results = results.map{ |point| compositeToPoint(point); };

    results.each{ |result|
        if testing[result].nil?
            puts("ERR #{real_results} vs #{results}");
            puts("IN #{listofthings} -> #{range}");
            puts("#{testing}");
            puts("tried to insert #{result}");
            raise "lol u suck";
        end

        testing[result] += 1;
    }

    if testing.any? { |k,v| v!=2 }
        puts("ERR #{real_results} vs #{results}");
        puts("IN #{listofthings} -> #{range}");
        puts("#{testing}");
        raise "lol u suck much!" ;
    end
    puts("OK #{results} vs #{range}");
end

def gen_list(max=10, dimensionality: 2)
    arrs = {};
    while arrs.size < max
        arr = [];

        dimensionality.times{
            arr << rand(max);
        }

        arrs[arr] = 1;
    end
    return arrs.keys;
end

def gen_range(max=10, dimensionality: 2)
    arr = [];
    dimensionality.times{
        d = rand(max);
        a = rand(max-d);
        b = a+max;
        arr << [a,b];
    }
    return arr;
end

while true
    size = 10;
    dimension = 2;
    
    listofthings = gen_list(size, dimensionality:dimension);
    range = gen_range(size, dimensionality:dimension);
    
    test(listofthings, range);
end