require_relative 'Task55';
require_relative '../MultiDReader';

def test(listofthings, position, radius)
    testing = {};
    real_results = listofthings.select{ |elem|
        (n_dist(elem, position) <= radius);
    };

    real_results.each{ |result|
        testing[result] = 1;
    }

    # Composite numbers
    listofthings = listofthings.map{ |point| pointToComposite(point); };

    tree = BuildKDTree(listofthings);

    results = SearchKDTree(tree,position,radius).map{|node| node.value};

    results = results.map{ |point| compositeToPoint(point); };

    results.each{ |result|
        if testing[result].nil?
            puts("ERR #{real_results} vs #{results}");
            puts("IN #{listofthings} -> #{position}, #{radius}");
            puts("#{testing}");
            puts("tried to insert #{result}");
            raise "lol u suck";
        end

        testing[result] += 1;
    }

    if testing.any? { |k,v| v!=2 }
        puts("ERR #{real_results} vs #{results}");
        puts("IN #{listofthings} -> #{position}, #{radius}");
        puts("#{testing}");
        raise "lol u suck much!" ;
    end
    puts("OK #{results} vs #{position}, #{radius}");
end

def gen_list(max=10, dimensionality: 2)
    arrs = {};
    while arrs.size < max
        arr = [];

        dimensionality.times{
            arr << rand(max);
        }

        arrs[arr] = 1;
    end
    return arrs.keys;
end

def gen_range(max=10, dimensionality: 2)
    arr = [];
    dimensionality.times{
        d = rand(max);
        a = rand(max-d);
        b = a+max;
        arr << [a,b];
    }
    return arr;
end

def gen_position(max=10, dimensionality: 2)
    arr = [];
    dimensionality.times{
        arr << rand(max);
    };
    return arr;
end

def gen_radius(max=10)
    return rand*max;
end

while true
    size = 10;
    listofthings = gen_list size;
    position = gen_position size;
    radius = gen_radius size;

    # listofthings = [[0, 0], [2, 2], [4, 4], [6, 6], [8, 8], [10, 10], [12, 12], [14, 14], [16, 16], [18, 18], [20, 20]];
    # position = [8, 0];
    # radius = 17;

    # tree = BuildKDTree(listofthings);

    # node = tree.right.left.left.right;
    # region = GetRegion(node);
    # puts("Node #{node} region is #{region}");

    # puts(IsFullyContainedWithin(node, position, radius));

    test(listofthings, position, radius);
end