require_relative 'Task56';
require_relative '../MultiDReader';

class MultiDReader
    def read_query
        # Get start and finish values
        points = gets.split(/(?<=\])\s/);

        points = points.map{ |s| s.gsub(/(\[|\])/,'').split.map{ |s| s.to_i } }; # remove [] and split

        # Output the result of the query
        @structure.query(*points).map{ |a| "[#{a.value.join(' ')}]"; }.join(' ');
    end
end

MultiDReader.new(Task56.new).read;