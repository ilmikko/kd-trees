# Exercise 2.4.
require 'benchmark';
require 'timeout';

require_relative './1.x/Task11.rb';
require_relative './1.x/Task12.rb';
require_relative './2.x/Task21.rb';

# * evaluate the time taken to construct lists of different sizes
# * evaluate the time taken on multiple queries for each list
#
# -> Average results, get standard error on the results
# -> Plot the results to a graph using a glorious spreadsheet software

$i=0;
def nice_print
	$i = ($i+1) % 10000;
	if $i == 0
		num = proc.();
		num = (num*100).to_i.to_s;
		num = "#{num}%";
		print("#{num}\e[#{num.length}D");
	end
end

class Benchmarker
	class Test
		def id
			"#{@id}-#{self.class.to_s.split('::').last}-#{@elements}";
		end

		private

		def log(bm)
			File.write("log/#{id}.log",bm.map{ |a| "%.30f" % a.real }.join("\n"));
		end

		def initialize(id, elements: 5, repeats: 10, timeout: 60)
			@id = id;
			@repeats = repeats;
			@timeout = timeout;
			@elements = elements;
		end
	end

	class Construction < Test
		def generate_input
			print("Generating input of size #{@elements} for construction...");

			input = [];
			
			(1..@elements).each{|i|
				input << i;
				nice_print{i.to_f / @elements};
			}

			puts("Done");

			return input.shuffle;
		end

		def run(task)
			begin
				const = Benchmark.bm{ |bm|
					@repeats.times{ |repeat|
						puts("#{id}: #{repeat.to_f / @repeats}");
						# Create a new data structure from the task class
						structure = task.new;

						# Generate random inputs for the elements
						input = generate_input;

						Timeout::timeout(@timeout) {
							bm.report("Construction (#{@elements} element(s)) -> "){
								input.each{ |number|
									structure.insert(number);
								}
								puts("Done");
							};
						}
					}
				}
				log(const);
			rescue Timeout::Error => e
				puts e;
			end
		end
	end

	class Query < Construction
		# save the old generate_inputs as we need it when querying the tree
		alias generate_numbers generate_input

		def generate_inputs()
			puts("Generating a pair of input(s) for querying...");

			nums = {};

			# Random size
			delta = rand(@elements);
	
			# Random starting pos
			a = rand(@elements-delta);
			b = a + delta;

			return [a,b];
		end

		def run(task)
			begin
				query = Benchmark.bm{ |bm|
					@repeats.times{ |repeat|
						# Create a new data structure from the task class
						puts("#{id}: #{repeat.to_f / @repeats}");
						
						structure = task.new;

						puts("Constructing tree of #{@elements} elements for querying...");
						# Generate the tree outside of our report
						structure.insert(*generate_numbers);

						start, finish = generate_inputs;
						# Report the query
						Timeout::timeout(@timeout) {
							bm.report("Querying #{@elements} element(s), #{start}..#{finish} -> "){
								puts("#{structure.query(start, finish).size}");
							};
						};
					}
				};
				log(query);
			rescue Timeout::Error => e
				puts e;
			end
		end
	end

	def run(id, task, elements, repeats)
		puts("Running benchmark for #{id} with #{elements} elements and #{repeats} repeat(s)");

		# Benchmarker::Construction.new(id, elements:elements, repeats: repeats).run(task);
		Benchmarker::Query.new(id, elements:elements, repeats: repeats).run(task);
	end

	private

	def initialize
	end
end

benchmarker = Benchmarker.new;

# benchmarker.run("Task 1.1", Task11, 5, 10);
# benchmarker.run("Task 1.1", Task11, 50, 10);
# benchmarker.run("Task 1.1", Task11, 500, 10);
# benchmarker.run("Task 1.1", Task11, 5000, 10);
# benchmarker.run("Task 1.1", Task11, 50000, 10);
# benchmarker.run("Task 1.1", Task11, 500000, 10);
benchmarker.run("Task 1.1", Task11, 5000000, 10);

# benchmarker.run("Task 1.2", Task12, 5, 10);
# benchmarker.run("Task 1.2", Task12, 50, 10);
# benchmarker.run("Task 1.2", Task12, 500, 10);
# benchmarker.run("Task 1.2", Task12, 5000, 10);
# benchmarker.run("Task 1.2", Task12, 50000, 10);
# benchmarker.run("Task 1.2", Task12, 500000, 10);
benchmarker.run("Task 1.2", Task12, 5000000, 10);

# benchmarker.run("Task 2.1", Task21, 5, 10);
# benchmarker.run("Task 2.1", Task21, 50, 10);
# benchmarker.run("Task 2.1", Task21, 500, 10);
# benchmarker.run("Task 2.1", Task21, 5000, 10);
# benchmarker.run("Task 2.1", Task21, 50000, 10);
# benchmarker.run("Task 2.1", Task21, 500000, 10);
benchmarker.run("Task 2.1", Task21, 5000000, 10);