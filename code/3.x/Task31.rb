# def dimension_binary_search(list, dimension, value, lo=0, hi=nil, direction: :left)
#     hi = list.length if hi.nil?;

#     puts("#{lo} #{hi} #{value}");

#     if hi-lo > 1
#         middle = lo+(hi-lo)/2;
        
#         if list[middle][dimension]==value
#             return middle
#         elsif list[middle][dimension]>value
#             return dimension_binary_search(list, dimension, value, lo, middle, direction: direction);
#         elsif value>list[middle][dimension]
#             return dimension_binary_search(list, dimension, value, middle, hi, direction: direction);
#         end
#     else
#         if direction == :left
#             return lo;
#         else
#             return hi;
#         end
#     end
# end

# def reduce_dimension(list, dimension, start, finish)
#     # Do not bother with an empty list
#     return list if list.empty?;

#     # Sort the list by the dimension:th coordinate
#     list = list.sort{|a,b| a[dimension]-b[dimension]};

#     # TODO: This is plain wrong
#     return [] if finish < list.first[dimension]; # don't even try
#     return [] if start > list.last[dimension]; # ditto

#     puts("#{list} #{dimension}");

#     # Get start and end nodes for querying
#     start_node = dimension_binary_search(list, dimension, start, 0, list.length, direction: :left);
#     end_node = dimension_binary_search(list, dimension, finish, 0, list.length, direction: :left);

#     puts("sn: #{start_node}, en: #{end_node}");

#     # Reduce the list
#     return list[start_node..end_node];
# end

# def n_dimension_query(list, min_max)
#     puts("N dimension query: #{list} vs #{min_max}");
#     (0...min_max.count).each{|dimension|
#         min, max = min_max[dimension];
#         puts("A:#{list}");
#         list = reduce_dimension(list, dimension, min, max);
#         puts("B:#{list}");
#     }
#     return list;
# end

def MultiDimensionQuery(list, min_max)
    list.select{ |node|
        # Are we still inside of the query?
        inside = true;

        (0...node.size).each{ |i|
            val = node[i];

            if !(min_max[i].first <= val and val <= min_max[i].last)
                # We don't have to loop anymore, as if the point is outside of one range in a dimension, it is outside of the whole range.
                inside = false;
                break;
            end
        };

        inside;
    };
end

class Task31
    def insert(*values)
        @list += values;
    end

    def query(min_max)
        return MultiDimensionQuery(@list, min_max);
    end

    private

    def initialize
        @list = [];
    end
end

#n_dimension_query([[1,2,3],[3,4,5],[2,1,4],[4,5,6],[5,6,7],[7,8,9],[100,100,20]],[[2,2],[1,3],[4,4]]);

# # Read input
# elements, dimensions, queries = gets.split.map{|s|s.to_i};

# point_list = [];

# (0...elements).each{|i|
#     point_list << gets.split.map{|s|s.to_i};
# };

# (0...queries).each{|i|
#     # Get all regex matches [1 2 3]
#     min, max = gets.scan(/\[(.*?)\]/).map{|group|
#         # Convert regex groups to arrays of integers
#         group[0].split.map{|s|s.to_i}
#     };

#     res = n_dimension_query(point_list, min.zip(max));
#     puts("#{res}");
# };