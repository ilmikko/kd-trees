require_relative '../OneDReader.rb';

class Node
    attr_accessor :left, :right, :value;

    def leaf?
        !!@leaf;
    end

    def subtree
        st = [];

        st += @left.subtree if !@left.nil?;
        st << @value if leaf?;
        st += @right.subtree if !@right.nil?;

        return st;
    end

    def put(node)
        if leaf?
            if @value.nil?
                # I don't have a value, so just put the node value here
                @left=node.left;
                @value=node.value;
                @right=node.right;
            else
                # I am a leaf and there is already something here, so I must become a node
                @leaf=false;

                if node.value<=@value
                    # As the old value is more than the new one, put it to the right
                    @right=Node.new(@value);
    
                    # Put the new value to the left and to the current node
                    @value=node.value;
                    @left=Node.new(node.value);
                else
                    # Become a node and create new leaves

                    # Right one becomes the bigger value
                    @right=Node.new(node.value);

                    # Left one becomes the current value
                    @left=Node.new(@value);
                end
            end
        else
            # I am a node, put to corresponding leaf
            if node.value<=@value
                # left
                @left.put(node);
            else
                # right
                @right.put(node);
            end
        end
    end

    def initialize(value=nil, leaf:true)
        # For a new node, the left leaf always contains its value.
        @leaf=leaf;
        @value=value;
    end
end

class Tree
    attr_reader :root;

    def initialize(*list)
        @root=Node.new(list.shift);
        list.each{|item|
            @root.put(Node.new(item));
        }
    end
end

def FindSplitNode(tree,start,finish)
    node = tree.root;

    while !node.leaf? and (finish <= node.value or start > node.value) do
        puts "#{finish} <= #{node.value} or #{start} > #{node.value}";
        if finish <= node.value then
            node = node.left;
        else
            node = node.right;
        end
    end

    puts("Split node is #{node.value}");

    return node;
end

def OneDRangeQuery(tree,start,finish)
    reported = [];

    split_node = FindSplitNode(tree,start,finish);
    if split_node.leaf? then
        # check if the point stored at split_node must be reported, and if so, report it
        reported << split_node if (finish >= split_node.value and start <= split_node.value);
        return reported;
    end

    # Follow the path to start and report the points in subtrees right of the path
    node = split_node.left;
    if !node.nil?
        right_reported = [];
        while !node.leaf? do
            if start <= node.value then
                right_reported = node.right.subtree + right_reported if !node.right.nil?;
                node = node.left;
            else
                node = node.right;
            end
        end;
        # check if v should be reported, and if so, it needed to be reported before the other subtrees
        reported << node.value if (start <= node.value and node.value <= finish);

        reported += right_reported;
    end

    # Follow the path to finish and report the points in subtrees to the left of the path
    node = split_node.right;
    if !node.nil?
        while !node.leaf? do
            if finish >= node.value then
                reported << node.left.subtree if !node.left.nil?;
                node = node.right;
            else
                node = node.left;
            end
        end;
        # check if v should be reported
        reported << node.value if (start <= node.value and node.value <= finish);
    end

    return reported;
end

class Task21
    def insert(value)

    end

    def query(start, finish)

    end

    private

    def initialize
        @tree = Tree.new;
    end
end

list = [];

elements, queries = gets.split.map{|s|s.to_i};

(0...elements).each{|i|
    # Store element from gets
    list << gets.to_i;
}

# Make a tree out of our list
tree = Tree.new(*list);

puts("#{tree.root.left.value}");

(0...queries).each{|i|
    # Query our list
    start, finish = gets.split.map{|s|s.to_i};

    # Output the result of the query
    puts(OneDRangeQuery(tree,start,finish).join(' '));

    puts(list.select{|a| start <= a and a <= finish}.sort.join(' '));
}