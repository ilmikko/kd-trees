require_relative '../OneDReader.rb';

def OneDRangeQuery(tree,start,finish)
    reported = [];

    split_node = FindSplitNode(tree,start,finish);
    if split_node.leaf? then
        # check if the point stored at split_node must be reported, and if so, report it
        reported << split_node if (finish >= split_node.value and start <= split_node.value);
        return reported;
    end

    # Follow the path to start and report the points in subtrees right of the path
    node = split_node.left;
    if !node.nil?
        right_reported = [];
        while !node.leaf? do
            if start <= node.value then
                right_reported = node.right.subtree + right_reported if !node.right.nil?;
                node = node.left;
            else
                node = node.right;
            end
        end;
        # check if v should be reported, and if so, it needed to be reported before the other subtrees
        reported << node.value if (start <= node.value and node.value <= finish);

        reported += right_reported;
    end

    # Follow the path to finish and report the points in subtrees to the left of the path
    node = split_node.right;
    if !node.nil?
        while !node.leaf? do
            if finish >= node.value then
                reported << node.left.subtree if !node.left.nil?;
                node = node.right;
            else
                node = node.left;
            end
        end;
        # check if v should be reported
        reported << node.value if (start <= node.value and node.value <= finish);
    end

    return reported;
end

def FindSplitNode(tree,start,finish)
    node = tree.root;

    while !node.leaf? and (finish <= node.value or start > node.value) do
        #puts "#{finish} <= #{node.value} or #{start} > #{node.value}";
        if finish <= node.value then
            node = node.left;
        else
            node = node.right;
        end
    end

    #puts("Split node is #{node.value}");

    return node;
end

class Node
    attr_reader :left, :right, :value;

    # Is this node a leaf?
    def leaf?
        !!@leaf;
    end

    # Get node's subtree
    def subtree
        st = [];
        
        # st += @left.subtree if !@left.nil?;     # Add the left node's subtree
        # st << @value if leaf?;                  # Add ourselves
        # st += @right.subtree if !@right.nil?;   # Add the right node's subtree

        node = @left;
        while !node.nil? and !node.leaf?
            st << node.value;
            node = node.left;
        end

        st << @value;

        node = @right;
        while !node.nil? and !node.leaf?
            st << node.value;
            node = node.right;
        end

        return st;
    end

    def put(node)
        if leaf?
            if @value.nil?
                # I don't have a value, so just put the node value here
                @left=node.left;
                @value=node.value;
                @right=node.right;
            else
                # I am a leaf and there is already something here, so I must become a node
                @leaf=false;

                if node.value<=@value
                    # As the old value is more than the new one, put it to the right
                    @right=Node.new(@value);
    
                    # Put the new value to the left and to the current node
                    @value=node.value;
                    @left=Node.new(node.value);
                else
                    # Become a node and create new leaves

                    # Right one becomes the bigger value
                    @right=Node.new(node.value);

                    # Left one becomes the current value
                    @left=Node.new(@value);
                end
            end
        else
            # I am a node, put to corresponding leaf
            if node.value<=@value
                # left
                @left.put(node);
            else
                # right
                @right.put(node);
            end
        end
    end

    private

    def initialize(value=nil, leaf:true)
        # A new node is always implicitly a leaf, unless otherwise stated.
        @leaf=leaf;
        @value=value;
    end
end

class Tree
    attr_reader :root;

    def put(value)
        if @root == nil
            # If there is no root node, make this our first.
            @root = Node.new(value);
        else
            # Otherwise let the root node handle this query
            @root.put(Node.new(value));
        end
    end

    private

    def initialize
        @root=nil;
    end
end

class Task21
    def insert(*values)
        values.each{ |value|
            @tree.put(value);
        }
    end

    def query(start, finish)
        return OneDRangeQuery(@tree, start, finish);
    end

    private

    def initialize
        @tree = Tree.new;
    end
end