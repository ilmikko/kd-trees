# Median finding algorithm

def median(list, k=nil, axis: 0)
    # Cannot find a median off of an empty list
    return nil if list.empty?;

    # If the list passed is an argument of form [1,2,3,4], redo with the zipped arguments.
    return median(list.zip,k,axis: axis) if !list.first.is_a? Array;

    len = list.length;

    # len-1 for rounding the uneven case down
    k = (len-1)/2 if k.nil?;

    if len <= 5
        return list.sort{ |a,b| a[axis]-b[axis]; }[k];
    else
        # Partition the list into subsets of 5.
        partitions = [];

        # Handle the even 5 length partitions
        for i in 0...len/5
            partitions << list[i*5...(i+1)*5];
        end

        # Handle the remaining partition if the divide wasn't even (the last one might contain 1-4 elements in this case)
        partitions << list[-(len%5)..-1] if len%5>0;

        # Select a median for each partition
        medians = partitions.map{|partition| median(partition,partition.length/2);};

        #puts("#{partitions} and #{medians}");

        # Get our friend master median m
        m = median(medians, len/10);

        # Partition the list into three groups depending on the relation to m
        less = [];
        equal = [];
        more = [];

        list.each{|item|
            if item[axis] < m[axis]
                less << item;
            elsif item[axis] > m[axis]
                more << item;
            else
                equal << item;
            end
        }

        # Run a recursive select over one of the partitions depending on where k falls
        # TODO: as we're running on unique elements we can probably get rid of equal altogether
        #puts("#{k} < #{less.count}");
        #puts("#{k} > #{less.count+equal.count}");
        #puts("#{less} #{equal} #{more}");

        if k < less.count
            # k <  less.count
            return median(less, k);
        elsif k > less.count
            # k >  less.count
            return median(more, k-less.count-1);
        else
            # k == less.count
            return m;
        end
    end
end

class Node
    attr_accessor :left, :right;
    attr_reader :value;

    def leaf?
        !!@leaf;
    end

    def inspect
        "<#{@value}>";
    end

    def subtree
        st = [];

        st += @left.subtree if !@left.nil?;
        st << self;
        st += @right.subtree if !@right.nil?;

        return st;
    end

    def initialize(value, leaf:false)
        @value=value;
        @leaf=leaf;

        @left=nil;
        @right=nil;
    end
end

def BuildKDTree(points, depth=0, dimensionality: nil)
    return nil if points.empty?;

    # Get the dimensionality from the first point
    dimensionality = points.first.length if dimensionality.nil?;

    # if P contains only one point
    return Node.new(points.first, leaf:true) if points.count==1;

    axis = depth % dimensionality;
    median = median(points, axis: axis);

    # Allocate the points in the tree to left / right side of the median
    left = [];
    right = [];
    points.each{|point|
        # if point[axis] <= median[axis]
        if point[axis] < median[axis]
            left << point;
        elsif point[axis] > median[axis]
            right << point;
        end
    }

    # Build the node representation
    node = Node.new(median);

    node.left = BuildKDTree(left,depth+1);
    node.right = BuildKDTree(right,depth+1);

    return node;
end

t = BuildKDTree([[2,3],[5,4],[4,7],[7,2],[8,1],[9,6]]);

puts("#{t.subtree}");