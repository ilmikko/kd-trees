#
# Reader for a multi dimensional range query (format described briefly before task 3.1.).
#

class MultiDReader
    def read_query
        # Get start and finish values
        min, max = gets.scan(/\[(.*?)\]/).map{|group|
            # Convert regex groups to arrays of integers
            group[0].split.map{|s|s.to_i}
        };

        min_max = min.zip(max);

        @structure.query(min_max).map{ |a| "[#{a.join(' ')}]"; }.join(' ');
    end

    def read
        # Reading input
        elements, dimensionality, queries = @input.gets.split.map{ |s| s.to_i };

        # Store each line of integers in our data structure
        (0...elements).each{ |i| @structure.insert(@input.gets.split.map{ |s| s.to_i }); }

        # Run the queries
        (0...queries).each{ |i|
            # Output the result of the query
            @output.puts read_query;
        }
    end

    private

    def initialize(structure, input: $stdin, output: $stdout)
        # Structure has (at least) two methods: insert(number) and query(start, finish)
        @structure = structure;

        # Redirection of streams for convenience
        @input = input;
        @output = output;
    end
end