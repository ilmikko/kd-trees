<div id="CS2521 Algorithmic Problem Solving"><h1 id="CS2521 Algorithmic Problem Solving">CS2521 Algorithmic Problem Solving</h1></div>
<div id="CS2521 Algorithmic Problem Solving-Assignment: Range Searching"><h2 id="Assignment: Range Searching">Assignment: Range Searching</h2></div>

<div id="CS2521 Algorithmic Problem Solving-Table of Contents"><h2 id="Table of Contents">Table of Contents</h2></div>

<ul>
<li>
<a href="index.html#Section 1">Section 1</a>

<ul>
<li>
<a href="index.html#1.1.">1 1</a>

<li>
<a href="index.html#1.2.">1 2</a>

</ul>
<li>
<a href="index.html#Section 2">Section 2</a>

<ul>
<li>
<a href="index.html#2.1.">2 1</a>

<li>
<a href="index.html#2.2.">2 2</a>

<li>
<a href="index.html#2.3.">2 3</a>

<li>
<a href="index.html#2.4.">2 4</a>

</ul>
<li>
<a href="index.html#Section 3">Section 3</a>

<ul>
<li>
<a href="index.html#3.1.">3 1</a>

<li>
<a href="index.html#3.2.">3 2</a>

</ul>
<li>
<a href="index.html#Section 4">Section 4</a>

<ul>
<li>
<a href="index.html#4.1.">4 1</a>

<li>
<a href="index.html#4.2.">4 2</a>

<li>
<a href="index.html#4.3.">4 3</a>

</ul>
<li>
<a href="index.html#Section 5">Section 5</a>

<ul>
<li>
<a href="index.html#5.1.">5 1</a>

<li>
<a href="index.html#5.2.">5 2</a>

<li>
<a href="index.html#5.3.">5 3</a>

<li>
<a href="index.html#5.4.">5 4</a>

<li>
<a href="index.html#5.5.">5 5</a>

<li>
<a href="index.html#5.6.">5 6</a>

</ul>
<li>
<a href="index.html#Figures">Figures</a>

</ul>

<div id="CS2521 Algorithmic Problem Solving-Section 1"><h2 id="Section 1">Section 1</h2></div>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1."><h3 id="1.1.">1.1.</h3></div>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Insertion"><h4 id="Insertion">Insertion</h4></div>

<p>
The insertion can be done using a linked list - we loop through the whole sorted list,
and every time we encounter a number that is larger than our current number,
we insert our number before it.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Insertion-Complexity of Insertion"><h5 id="Complexity of Insertion">Complexity of Insertion</h5></div>

<p>
Insertion to (the front of) a linked list is effectively a \(O(1)\) operation,
or a \(O(n)\) operation for \(n\) elements.
The insertion will also be \(Ω(n)\) as we (obviously) cannot do better than \(n\) insertions for \(n\) elements.
This makes the algorithm complexity \(Θ(n)\) for adding \(n\) items to the list.
</p>

<p>
A linked list needs two memory cells per item, one storing the value and one storing the pointer to the next element.
The amount of memory cells used is linearly dependent on the amount of items stored, so the memory complexity for it is \(Θ(n)\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Insertion-Correctness of Insertion"><h5 id="Correctness of Insertion">Correctness of Insertion</h5></div>

<p>
The algorithm correctly inserts an element into the front of the list.
Elements need not be ordered, so inserting to the front is acceptable.
Querying and creating the first node is a constant time operation, and there are no loops involved.
This means that the algorithm will always terminate.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Insertion-Pseudocode for Insertion"><h5 id="Pseudocode for Insertion">Pseudocode for Insertion</h5></div>

<pre>
Input:
	A linked list node that contains a reference that can be followed to get to the end of the whole linked list
	A second, single node that needs to be inserted
Output:
	nil
function insert(linked_list, node)
		old_first := linked_list.first
		linked_list.first = node
		node.next = old_first
end function
</pre>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Querying"><h4 id="Querying">Querying</h4></div>

<p>
For querying, we need to consider all of the elements in the linked list, which already makes it an \(O(n)\) operation with n elements in the list.
We loop through each item in the list and check whether the number is in the range specified, and if so, append to the list.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Querying-Complexity of Querying"><h5 id="Complexity of Querying">Complexity of Querying</h5></div>

<p>
Checking whether an item is within a range is a \(O(1)\) operation, so the final complexity for \(n\) elements will be \(O(n)\).
The querying will also be \(Ω(n)\) as we cannot do better than \(n\) range checks with a list of \(n\) items; we need to consider every element of the list at least once
in order to determine whether an item is in the range or not.
This makes the algorithm \(Θ(n)\) for querying the list.
</p>

<p>
The memory complexity of querying the linked list is still \(Θ(n)\) as we are potentially only making copies of all of the pointers to the linked list (requiring \(O(2n) = O(n)\) memory).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Querying-Correctness of Querying"><h5 id="Correctness of Querying">Correctness of Querying</h5></div>

<p>
Checking whether an item is within a range:
Being 'in range' means that the inequality \(x_s ≤ a ≤ x_f\) for an item \(a\) that we are considering, and a range of the form \(r = (x_s,x_f)\) where \(x_s ≤ x_f\).
We will consider every item in the list, only returning those items that fall inside this range. Thus, the algorithm returns a correct result.
The algorithm loops through a finite list, meaning that at some point it will run out of items to consider, and terminate.
This means the whole query terminates. These points considered, the algorithm is correct.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.1.-Querying-Pseudocode for Querying"><h5 id="Pseudocode for Querying">Pseudocode for Querying</h5></div>

<pre>
Input:
	A linked list node that contains a reference that can be followed to get to the end of the whole linked list
	A number that indicates the start of the range query
	A number that indicates the end of the range query
Output:
	A list of nodes that were in the query range
function query(linked_list, start, finish)
		for each item in linked_list
				if start ≤ item ≤ finish
					report this item
				end if
		end for
		
		return reported items
end function
</pre>

<p>
Please see <code>Task11.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2."><h3 id="1.2.">1.2.</h3></div>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Insertion"><h4 id="Insertion">Insertion</h4></div>

<p>
The complexities required can be achieved using a different implementation where the list is already in a sorted form.
This means that every time we need to insert a new element, we need to reorder the rest of the elements into a sorted form.
The sorting enables us to query numbers in our range much more efficiently, and find the starting / ending points of the range in the sorted list.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Insertion-Complexity of Insertion"><h5 id="Complexity of Insertion">Complexity of Insertion</h5></div>

<p>
Insertion to this sorted list is a \(O(n\log{n})\) operation.
We can insert a new element into the end of a self-resizing array in (amortized) \(O(1)\) time, and then sort the new array
using an efficient \(Θ(n\log{n})\) sorting algorithm, like merge sort. Insertion to the array takes also \(Ω(1)\) time, making it a \(Θ(1)\) operation.
The combined time complexity thus will be \(Θ(n\log{n})+Θ(1) = Θ(n\log{n})\) for inserting a new element into the list.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Insertion-Correctness of Insertion"><h5 id="Correctness of Insertion">Correctness of Insertion</h5></div>

<p>
Inserting into the end of the list and then sorting the list will produce the desired output provided that our chosen sorting algorithm does its job.
Merge sort is an algorithm where a larger list of items is divided into smaller chunks, and these chunks are then sorted, and finally merged together.
The sorting part happens at the base case where there are two lists of small length, and these are merged together, taking the smallest elements from both lists first.
As the small lists are already sorted, merging two of them will produce a larger sorted list. This is repeated until all of the small lists have been used, and we are left with a sorted list.
</p>

<p>
Inserting into the end of our list always terminates regardless of the list's size.
Merge sort is a divide and conquer algorithm, which means operating on a finite set of items it will terminate. Thus our algorithm also terminates.
</p>

<p>
This means our insertion algorithm is correct.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Insertion-Pseudocode for Insertion"><h5 id="Pseudocode for Insertion">Pseudocode for Insertion</h5></div>

<pre>
Input:
	A dynamically resizing list of nodes
	A node to insert
Output:
	nil
function insert(list, item)
	list[list.length] := item
	list = mergesort(list)
end function

Input:
	A list of items
Output:
	A sorted list of items
function mergesort(list)
	if there is only one element in the list
		return the list
	end if
	
	// Get the split index
	middle := length(list)/2
	
	// Partition into two halves
	left := list from start to middle (exclusive)
	right := list from middle (inclusive) to end
	
	// Sort the lists
	left := mergesort(left)
	right := mergesort(right)
	
	return merge(left, right)
end function

Input:
	A sorted list of items
	Another sorted list of items
Output:
	A combined sorted list of items
function merge(list1, list2)
	merged := empty list
	
	// Merge the values from two lists
	while list1 and list2 are not empty
		if list1.first &lt;= list2.first
			append list1.first to merged
			pop list1.first out of list1
		else
			append list2.first to merged
			pop list2.first out of list2
		end if
	end while
	
	// Merge the rest of the lists, as the other one has 'ran out'
	if list1 is not empty
		append the rest of list1 to merged
	else
		append the rest of list2 to merged
	end if
	
	return merged
end function
</pre>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Querying"><h4 id="Querying">Querying</h4></div>

<p>
The sorting enables us to use binary search for the starting point of the range, and loop through the amount of items we need, until we get 
Querying the list will be easy: we get the start of the range we need, and find the corresponding
element using binary search.
Then we loop through the array until the next element is going to be more than the end range.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Querying-Complexity of Querying"><h5 id="Complexity of Querying">Complexity of Querying</h5></div>

<p>
Finding the start of the range takes \(O(\log{n})\) time, as per usual for a binary search. This is because binary search is a divide and conquer algorithm; it halves the problem set each time, and considers a smaller set of numbers. Thus, for \(n\) items, there must be at least (and at maximum) \(\log{n}\) halving steps before the value can be found.
This means that the binary search part takes \(Θ(\log{n})\) time to complete for a list of \(n\) items.
As the range checking happens in a simple loop which ends as the first unwanted element is found, looping through the wanted elements takes \(O(k)\) time where \(k\) is the amount of items we want returned.
We also cannot do better than \(Ω(k)\), as every item that is inside this range needs to be considered. Because of the nature of the array, though, we can discard all of the other elements when we find the first that exceeds our query; we know that the rest would as well.
This means that the query will have a combined \(Θ(\log{n}) + Θ(k) = Θ(k+\log{n})\) runtime.
</p>

<p>
The memory complexity for sorting and binary search is \(O(n)\), as we need to only consider copies of pointers to the list when returning the reported items.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Querying-Correctness of Querying"><h5 id="Correctness of Querying">Correctness of Querying</h5></div>

<p>
The reported list starts with a start index found by binary search and ends with the last element that was in the range.
Because we are working with a sorted array, we can agree that anything before the start index cannot be in the desired range*.
The next step is to loop through the elements in the list until the first element that doesn't fall in the range, or the end of the list, is encountered.
If the algorithm finds a value that does not fall in the range (more precisely, is larger than the range maximum), the loop breaks. This is because in a sorted list, we can assume that the rest of the values are also going to fail the range check, as they will be over the maximum as well.
*An interesting case to note is when the searched value is over the largest element of the list. This is when the binary search will indeed return a value below the value searched for. However, in the next step the range check fails, and the loop breaks, as there are no more items that can be inside the range.
</p>

<p>
The binary search will terminate eventually, as the step size starts from a finite number and will get smaller. The recursion stops when the step size reaches a value less than one - this means we have settled on an index and will return that.
Because we are iterating over a limited set of items, the range checking loop must also terminate. Thus the algorithm terminates, and is correct.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 1-1.2.-Querying-Pseudocode for Querying"><h5 id="Pseudocode for Querying">Pseudocode for Querying</h5></div>

<pre>
Input:
	A list of nodes
	A number that is the start of our range query
	A number that is the end of our range query
Output:
	A list of nodes that are inside the range query
function query(list, start, finish)
	start_index := binarysearch(list, start, 0, length(list)-1)

	reported := []

	for item from start_index to list.last_index
		if item in range
			add item to reported items
		else
			break out of loop
		end if
	end for

	return reported items
end function

Input:
	A list of nodes
	A node value to search for
	A number indicating the lowest index that we are searching in
	A number indicating the highest index that we are searching in
Output:
	A number indicating the index where the value can be found at
function binarysearch(list, value, lo, hi)
	// First, check if our step size is still meaningful.
	// If the step size is 0, this means we have exhausted our options and the value does not exist in the list.
	// If that happens, just return the index at hi, meaning the item closest to our value, but over.
	if hi-lo &gt; 1
		// Get the new middle point
		middle := lo+(hi-lo)/2
		
		if list[middle]==value
			// If the value has been found, return it.
			return middle
		else if list[middle]&gt;value
			// The middle value was too large, so it must be a high point.
			return binarysearch(list, value, lo, middle)
		else if list[middle]&lt;value
			// The middle value was too small, so it must be a low point.
			return binarysearch(list, value, middle, hi)
		end if
	else
		return hi;
	end if
end function
</pre>

<p>
Please see <code>Task12.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2"><h2 id="Section 2">Section 2</h2></div>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.1."><h3 id="2.1.">2.1.</h3></div>

<p>
Please see <code>Task21.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.2."><h3 id="2.2.">2.2.</h3></div>

<p>
Inserting an element into the tree has a complexity of \(O(l)\) where \(l\) is the number of layers in the tree, as all of these layers need to be considered.
This is because at worst, you will find a leaf at the very bottom of the tree. When building the tree, the worst list to pass is an already sorted one.
When a value that is being put into a node is larger than the value of that node, it will get passed to the right side of that node.
</p>

<p>
For a sorted list, every next value is going to be larger than the previous one, which results in a "right-leaning" tree;
that is, for every node in the tree that is not a leaf, that node will only have a right branch to another node.
This means that our 'tree' structure is effectively reduced into a list, and traversing to the last node will take \(O(n)\) time for \(n\) elements.
</p>

<p>
As inserting an item takes \(Θ(n)\) time, inserting \(m\) items will take \(Θ(n*m)\) time, as all of the items will be inserted into the bottom of the tree (and a new element will of course go past the next one, and so on).
</p>

<p>
Starting with an empty tree, we are inserting \(m\) elements, and after each insertion there will be one more element to go through, until the last element which will take \(O(m-1)\) time to insert as it has to pass through all of the previously inserted elements. 
This means that initially building the tree will have \(Θ(n*m) = Θ(n^2)\) complexity, as it is sufficient to say that \(n=m\) in this case.
</p>

<p>
If a self-balancing tree was used instead, we could achieve amortized \(Θ(\log{n})\) insertion time, as the structuring of the tree prevents the tree to become "right-leaning" in the worst case. This would mean insertion of \(n\) elements initially would only take \(Θ(n\log{n})\) time.
</p>

<p>
The memory complexity of building a tree is merely \(Θ(n)\), as the number of nodes in the binary tree can not exceed the number of stored leaves, and we store only \(Θ(n)\) leaves.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.3."><h3 id="2.3.">2.3.</h3></div>

<p>
Performing a single query using the <code>OneDRangeQuery</code> algorithm
The algorithm has four parts. We will look at each part on their own, and then combine the complexities.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.3.-Complexity of FindSplitNode"><h4 id="Complexity of FindSplitNode">Complexity of FindSplitNode</h4></div>

<p>
Finding the split node has a loop from the root node to the first node that is a leaf or is included in our range specification.
</p>

<p>
Finding out the root of the tree is a \(Θ(1)\) operation.
</p>

<p>
The worst case complexity for the loop is \(O(l)\) where \(l\) is the number of layers in the tree, because we would have to travel through all of the layers of the tree to find our matching node, and return that node.
In a balanced binary tree, this would evaluate to \(Θ(\log{n})\) time. In the worst case, finding the split node will take \(O(n)\) time for \(n\) items in the tree.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.3.-Complexity of traversing a subtree path"><h4 id="Complexity of traversing a subtree path">Complexity of traversing a subtree path</h4></div>

<p>
The complexity of traversing a subtree path is \(Θ(k)\) for \(k\) reported nodes of the range query.
This is because in a tree, the amount of leaves is always greater than the amount of nodes before those leaves.
Traversing a subtree can be done using depth first search, and the complexity of that is limited by how many leaves there are to report, as this linearly correlates to the number of nodes traveled.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.3.-Adding all the complexities together"><h4 id="Adding all the complexities together">Adding all the complexities together</h4></div>

<p>
The total complexity of one <code>OneDRangeQuery</code> method call is thus as follows:
\(Θ(\log{n})\) for finding the split node
\(Θ(k)\) for reporting the subtree paths from that split node
</p>

<p>
Thus the total complexity will be: \(Θ(\log{n}) + Θ(k) = Θ(\log{n}+k)\) for one ran instance of <code>OneDRangeQuery</code>.
The memory complexity of a query is \(Θ(k)\) for the amount of nodes reported \(k\), as no additional memory to the tree will be required.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.4."><h3 id="2.4.">2.4.</h3></div>

<p>
Using the standard Ruby benchmarking library, 1000 repeats of each test were performed with randomized queries, and the resulting runtimes were plotted against the number of elements manipulated.
In constructing the tree, a list of numbers from \(0\) to \(n\) was shuffled randomly, where \(n\) is the number of elements we are considering.
For querying, the same tree construct was used, but the query start and end values were randomized, keeping in mind that \(x_s ≤ x_f\) and \(0 ≤ x_s ∧ x_f ≤ n\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 2-2.4.-Discussion"><h4 id="Discussion">Discussion</h4></div>

<p>
The data structure described in Task 1.1 has the fastest insertion time, because it is essentially a linked list. However, there is a tradeoff for the fast insertion time; when querying large sets of items, the time taken grows linearly against \(n\) (hence \(O(n)\) for querying), and thus making Task 1.1 the slowest algorithm to query on.
</p>

<p>
Task 1.2 provides a faster query; this is because of the exploitation of the binary search algorithm to find the starting element instead of looping through each element.
However, this as well comes at a cost: when inserting an element into the list, it needs essentially to be sorted again, causing an \(O(n\log{n})\) complexity for insertion.
If a naïve algorithm is used for inserting \(n\) elements, the complexity becomes \(O(n^2\log{n})\) for inserting \(n\) elements into the list, the result of which can be seen in Fig. 1.
The data structure of Task 1.2 begins to take over 30 seconds to generate for \(n &gt; 5000\), and the time continues to grow exponentially further on. There is a remedy for inserting multiple elements to this list, however.
We can simply sort the list only once after inserting all of the \(n\) elements, instead of sorting after every single insertion. This will reduce the insertion complexity to \(Θ(n\log{n})\) for \(n\) elements.
</p>

<p>
Finally, the algorithm described in Task 2.1 provides a slightly more inefficient data structure to build, but its' querying speed is the fastest of the compared three.
While building the tree takes initially more time than simply inserting into a list, repeated queries can be performed very efficiently. This makes the data structure of Task 2.1 good for applications where the amount of queries greatly exceeds the amount of insertions into the data structure.
Similarly Task 1.2., if the remedy discussed before was applied, could be used for instances where fast querying is needed, but the insertion can be slow.
Task 1.1 could be used in quite the opposite way, where fast insertion is essential but the data can be queried later, or optionally transformed into another structure that supports faster querying.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 3"><h2 id="Section 3">Section 3</h2></div>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.1."><h3 id="3.1.">3.1.</h3></div>

<p>
A very simple solution to perform a multidimensional range query on a set of points is to merely loop through this list of points, and check whether the coordinate on each dimension is inside the range query for that particular dimension. Moreso, if a range query succeeds for each dimension for a specific point, we can say that this point is inside the range for all those dimensions.
For example, a point is inside a range rectangle if and only if it's x-coordinate is between the rectangle's left wall and it's right wall, and it's y-coordinate is between the rectangle's top wall and it's bottom wall.
</p>

<p>
More formally, a point \(v\) of form \(v = (x_0, x_1, ..., x_d)\) where \(d\) is the number of dimensions is inside a range query \(q\) of form \(q = ((s_0,f_0), (s_1,f_1), ..., (s_d,f_d))\) iff \(∀z(s_z ≤ x_z ≤ f_z)\) where the u.d. is the coordinates in each dimension for point \(v\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.1.-Pseudocode for 3.1. algorithm"><h4 id="Pseudocode for 3.1. algorithm">Pseudocode for 3.1. algorithm</h4></div>

<pre>
Input:
	A list of nodes, each node being of form (x_0, x_1, ... x_d), depending on the number of dimensions.
	A list of ranges for each dimension, of form ((x_min,x_max),(y_min,y_max),...), number of ranges depending on the number of dimensions.
Output:
	A list of nodes matching the range query
function MultiDimensionQuery(list, min_max)
	for each node in list
		inside := true
		
		// Loop through all dimensions and check if the range query matches
		for each dimension in node and min_max
			min, max = min_max[dimension]
			val = node[dimension]
			
			if not min &lt;= val &lt;= max then
				// If the range query fails for a dimension, we don't have to loop anymore.
				// We can deem the point outside of our query range in all dimensions combined, if one dimension fails.
				inside := false
				break
			end if
		end for
		
		// Report the node if it is inside the range query
		if inside
			report node
		end if
	end for
	return reported nodes
end function
</pre>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.1.-Footnote: on a more efficient query algorithm"><h4 id="Footnote: on a more efficient query algorithm">Footnote: on a more efficient query algorithm</h4></div>

<p>
There is a solution which is more efficient to query, but requires more space and is less efficient (\(Θ(n\log{n})\)) to set up.
This solution bases on the fact that we can use binary search to find the starting point of our query in our list and loop through \(k\) items that are going to be returned in our query, rather than having to loop through the whole list of potentially many elements.
However, this requires the list to be sorted on each dimension beforehand, which means keeping track of \(d\) lists. This increases the memory complexity from \(Θ(n)\) to \(Θ(dn)\). Moreso, sorting itself will take \(Θ(n\log{n})\) time for each dimension of space.
</p>

<p>
Using this method querying can be improved from \(Θ(n)\) to \(Θ(\log{n}+k)\) complexity. This would be a good candidate for an situation where querying is frequent and insertion infrequent.
</p>

<p>
As a simple algorithm was requested, I have provided pseudocode and implement the more inefficient query. Hopefully one can implement the query algorithm proposed here using the above description, given enough time and vigour.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.2."><h3 id="3.2.">3.2.</h3></div>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.2.-Correctness of the 3.1. algorithm"><h4 id="Correctness of the 3.1. algorithm">Correctness of the 3.1. algorithm</h4></div>

<p>
An algorithm is correct if two conditions apply.
</p>

<ol>
<li>
The algorithm must produce the correct output in all legal instances

<li>
The algorithm must terminate

</ol>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.2.-Correctness of the 3.1. algorithm-I. Correct output"><h5 id="I. Correct output">I. Correct output</h5></div>


<p>
Let us take a look at the more efficient algorithm described in 3.1.
</p>

<p>
Take any array \(a\) where \(|a| = d\) and a dimension \(d\) where \(d ∈ \mathbb{N}_+ ∧ d≠0\) and the items are nodes \(n\) of form \((x_1, x_2, ..., x_d)\).
</p>

<p>
Let range \(r\) be a tuple of form \((x_s, x_f)\) where \(\{x_s, x_f\} ⊆ \mathbb{R}\) and \(x_s &lt; x_f\).
For \(d\) dimensions, we need to have an array \(b\) where \(|b| = d\) and all the items of \(b\) are ranges of form \(r\).
This is a representation of our range 'rectangle'. Across \(d\) dimensions, this area can be defined as starting from \(x_s\) and ending to \(x_f\) for each of our \(r ∈ b\).
</p>

<p>
We want our algorithm to return the nodes inside this rectangle.
This means that the algorithm should return all nodes \(n\) where \(x_s ≤ x_d ≤ x_f\) where \(x_d ∈ a\) for all \(0\) to \(d\).
</p>

<p>
We can check the base case for the algorithm where \(d = 1\). This means we get the first element \(r\) from the array \(b\) to indicate the range we want from those nodes.
We then sort our array of nodes \(a\) according to the first coordinate \(x_1\) for every node \(n ∈ a\) so that we can use binary search on this array.
</p>

<p>
Using binary search, we find the first node \(n_s\) with the first coordinate \(x_1\) that satisfies the requirement \(x_1 ≥ x_s\).
We also find the last node \(n_f\) that's first coordinate satisfies the requirement \(x_1 ≤ x_f\).
Because the array is sorted, we know that all nodes \(n_{range} ⊆ (n_s, n_s+1, ..., n_f)\) also satisfy the requirement.
</p>

<p>
Because it is true that \(∀x(x_s ≤ x ≤ x_f)\) in \(n_{range}\), we have proven the base case to hold for our algorithm.
For multiple dimensions, we will prove the correctness via mathematical induction.
</p>

<p>
Assuming that our algorithm holds for dimension \(d_{prev} = d-1\), we will prove that it holds for \(d\).
For \(d\)th dimension, we will get \(r_d\) where \(r_d ∈ b\), as we know that \(|b| = d_{max}\), so we always have \(x_s\) and \(x_f\) to retrieve.
</p>

<p>
Again, we can find the \(d\)th coordinate \(x_d ∈ a\) that satisfies the requirement \(x_d ≥ x_s\). Remember that \(|a| = d\), so we will always have an \(x_d\) to retrieve from a node.
</p>
 
<p>
We can use binary search to find the first node \(n_s ∈ a\) with a coordinate \(x_d\) that satisfies the requirement \(x_d ≥ x_s\).
In a similar way we can find \(n_f ∈ a\) with a coordinate \(x_d\) that satisfies \(x_d ≤ x_f\).
Again, as the array is sorted, \(n_{range} ⊆ (n_s, n_s+1, ..., n_f)\) also satisfy our requirement.
</p>

<p>
Because it is true that \(∀x(x_s ≤ x ≤ x_f)\) in \(n_{range}\), we have proven the inductive case to hold, which means our algorithm holds for any input size \(n ≥ 1 ∧ n ∈ \mathbb{N}_+\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.2.-Correctness of the 3.1. algorithm-II. Termination"><h5 id="II. Termination">II. Termination</h5></div>

<p>
An iteration of the algorithm has to sort a finite list of nodes.
The efficient sorting algorithm (merge sort in this case) operates on a finite list, and terminates.
The algorithm also uses binary search, again on a finite list.
As binary search is a divide and conquer - type algorithm, eventually the list will be divided to its indivisible atoms and will terminate.
Lastly, the algorithm will return a subset of the original list. As the original list is already finite, a subset of it will also be finite, and thus cloning that subset will terminate.
As an iteration of the algorithm consists of these steps which will all terminate, an iteration of the algorithm also always terminates.
</p>

<p>
The algorithm works on a finite number of dimensions. After every iteration, there are fewer remaining dimensions to check.
Thus, eventually, the algorithm must halt as there is a finite number of iterations.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 3-3.2.-Worst case complexity for 3.1."><h4 id="Worst case complexity for 3.1.">Worst case complexity for 3.1.</h4></div>

<p>
The algorithm described in 3.1. needs to loop through \(n\) elements to check if they are inside the range query, so the complexity will be \(O(n)\).
The list does not need any additional preparation, so building the list initially is a \(O(n)\) operation (insertion of one element is \(O(1)\) to a list)
All of the range checks run in \(O(1)\) time, and the bottleneck is looping through each element of the list.
</p>

<p>
This can be mitigated and performance improved by preparing the lists beforehand (as mentioned in the footnote). This will reduce the worst case complexity for querying down to \(O(\log{n}+k)\) where \(k\) is the elements returned by the query. However, this requires preparation beforehand, more precisely \(O(n\log{n})\) for sorting the list, repeated for each dimension.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 4"><h2 id="Section 4">Section 4</h2></div>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.1."><h3 id="4.1.">4.1.</h3></div>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.1.-Finding a median point on a list of nodes"><h4 id="Finding a median point on a list of nodes">Finding a median point on a list of nodes</h4></div>

<p>
Pick a dimension \(d\) and an unsorted list of nodes. We can use a divide and conquer algorithm to calculate the median of this list in \(Θ(n)\) time.
</p>

<p>
The algorithm is as follows: 
</p>
 
<p>
If the array has \(≤ 5\) elements, sort the array by dimension \(d\) and return the element in the middle (or, in case of even elements, the one left to the middle point).
Split the list into \(n/5\) groups of \(5\) nodes each, and sort these small lists by dimension \(d\).
Second, we find the true median of these lists, by selecting the middle element(s) in the sorted lists.
Find the expected median \(M\) ...
Partition the list into three sets \(A\), \(B\) and \(C\), where items in \(A &lt; M\), \(B = M\) and \(C &gt; M\)
If \(k ≤ |A|\), we recurse the selection algorithm again with the set \(A\).
if \(k &gt; |A|+|B|\), we recurse using the set \(C\).
else, we have found our median point which is \(M\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.1.-Complexity of the algorithm"><h4 id="Complexity of the algorithm">Complexity of the algorithm</h4></div>

<p>
As we are sorting such a small number of items, the sorting step can be considered to be constant time. 
Because in most cases \(5 \ll n\), we can say that the splitting and sorting step takes \(Θ(n)\) time.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.2."><h3 id="4.2.">4.2.</h3></div>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.2.-Time complexity of one iteration"><h4 id="Time complexity of one iteration">Time complexity of one iteration</h4></div>

<p>
Looking at the pseudocode, we can calculate what the complexity of one iteration of <code>BuildKDTree</code> is.
Building objects and getting a modulo can be all considered as \(Θ(1)\) operations.
</p>

<p>
Finding the median of a list, using the algorithm described and proven before, is an \(Θ(n)\) operation.
</p>

<p>
In order to determine the left and right side of the node (\(P_l\) and \(P_r\), respectively), we have to consider every node in the set at least once.
This means that the best case scenario is \(Ω(n)\), and the worst case scenario for a regular loop is \(O(n)\).
Checking both the sides is therefore \(Θ(2n) = Θ(n)\).
</p>

<p>
Thus, the combined complexity for one iteration of <code>BuildKDTree</code>, ignoring the recurrence, is \(Θ(n)\)
</p>

<p>
(Technically, when \(|P|=1\), the complexity of the algorithm will be \(Θ(1)\), but that can be overlooked as in that case \(n=1\) holds true as well.)
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.2.-Complexity of the recurrence"><h4 id="Complexity of the recurrence">Complexity of the recurrence</h4></div>

<p>
Now that we know the complexity of one iteration of <code>BuildKDTree</code>, we can find out what the complexity of the whole recurrence is.
</p>

<p>
It makes our life easier to assume the median point is found somewhere near the center of the array, meaning that the split will be nearly even.
This makes the \(n\) in our recurrence halve each time, and simplifies our recurrence.
</p>

<p>
The total cost \(BuildKDTree(n) = BuildKDTree(n/2) + BuildKDTree(n/2) + Θ(n)\).
This can be simplified as just \(T(n) = 2*T(n/2) + Θ(n)\).
</p>

<p>
We can use the master method to find out the complexity for this recurrence.
The master method works for recurrences of the form \(T(n) = aT(n/b)+f(n)\).
We can see that in this specific case \(a=2\), \(b=2\), \(f(n) = n\).
</p>

<p>
We can see that \(f(n) = Θ(n^{\log_2{2}}) = Θ(n^1) = Θ(n)\), so the second condition of the master method holds.
</p>

<p>
This means that \(T(n) = Θ(n^{\log_2{2}}\log{n}) = Θ(n\log{n})\) for any sufficiently large \(n\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.2.-Storage Complexity"><h4 id="Storage Complexity">Storage Complexity</h4></div>

<p>
As for the storage complexity, the tree grows linearly per every node inserted; every node will use \(Θ(n)\) storage. Thus, the total storage complexity is \(Θ(n)\) for \(n\) items.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 4-4.3."><h3 id="4.3.">4.3.</h3></div>

<p>
Please see <code>Task41.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5"><h2 id="Section 5">Section 5</h2></div>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.1."><h3 id="5.1.">5.1.</h3></div>

<p>
Please see <code>Task51.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.2."><h3 id="5.2.">5.2.</h3></div>

<p>
A node (that is not a leaf) in the KD-tree has two regions; one on it's left side, and one on it's right side.
The right region is bound by the node's coordinate on the left, but open on the right, and vice versa for the left region.
The accumulation of these regions may sometimes create finite bounds for some of the children, and the proposed algorithm will check whether these bounds are inside the original range query.
</p>

<p>
More precisely: 
</p>

<p>
Take a node \(v\) splitting an axis \(v_d\) a dimension \(d\). This node has two regions, the left region \(R_l(n)\) and the right region \(R_r(n)\).
The left region contains nodes \(o ∈ R_l(n)\) that always have \(o_d &lt; v_d\), and the right region contains nodes \(o ∈ R_r(n)\) that always have \(v_d &lt; o_d\).
We also have our range query \(r\) of form \((v_s,v_f)\) where \(x_s ≤ x_f\) for every coordinate \(x ∈ v\) the current dimension \(d\).
</p>

<p>
First we need to check whether our node \(v\) falls inside the query range for the current dimension \(d\).
If it doesn't, then neither of the two regions cannot be fully contained in the query range, and our check is finished.
</p>

<p>
We also need to define a term called <em>axis parent</em>, the first parent node \(w\) for a node \(v\) where the two nodes share the same splitting axis, or \(v_d = w_d\).
Some nodes do not have an axis parent - this means that the node's region is unbound on the current axis, and thus the region cannot be fully contained (see Fig. 7).
</p>

<p>
Let us look at a case where \(x_s ≤ v ≤ x_f\), focusing on the <span id="CS2521 Algorithmic Problem Solving-Section 5-5.2.-left"></span><strong id="left">left</strong> region \(R_l(n)\).
Let node \(n_{ap}\) be the axis parent for node \(v\) when dealing with \(d_{max}\) dimensions, we need to traverse \(d_{max}\) parents to find the next axis parent for a node.
If node \(w\) has the property \(x_s ≤ w ≤ x_f\), then the <span id="CS2521 Algorithmic Problem Solving-Section 5-5.2.-right"></span><strong id="right">right</strong> region \(R_r(m)\) of the node \(w\) will be fully contained on the axis for dimension \(d\).
</p>

<p>
We will repeat this step for \(d_{max}-1\) parents, so that we have checked over all of the dimensions. If the region is bound over all of these dimensions, then the node's regions are fully contained within the range query.
This can be repeated for the right region \(R_r(n)\).
</p>

<p>
Let us consider a concrete example to comprehend this algorithm better.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.2.-An example"><h4 id="An example">An example</h4></div>

<p>
In the tree (Fig. 3), we are considering node \(n\) and dimension \(d_l = 0\) (x-axis), given any layer \(l ≥ 0\).
We can see that \(x_s ≤ o_v ≤ x_f\), which means we need to consider both regions as they might be fully contained.
In order to keep the example simple, let us consider only the left region \(R_l(n)\) as it is more interesting.
We loop through the subtree of the left child of node \(n\), and find the next nodes \(M ⊆ R_l(n)\) where dimension \(d_m = 0\), \(∀m(m ∈ M)\).
</p>

<p>
Next, we notice that a grandchild node \(m ∈ R_l(n)\) is also contained within the range.
The nodes \(m\) and \(n\) restrict a range on the x-axis that can be seen highlighted in red (Fig. 4).
This means that the right region is fully contained within the range - on dimension \(d\).
In order to check whether the whole region is contained, we need to consider all the dimensions.
</p>

<p>
The second step is essentially same than the first step, only working on one layer deeper than the first one (\(l+1\) instead of \(l\)).
This is because our KD-tree modulates between the dimensions: if \(l\) is on axis x, then \(l+1\) is on axis y, \(l+2\) is on axis x again, etc...
We again check if \(n\) is contained within the range for dimension \(d_{l+1}\).
We also notice that there exists a grandchild node \(m ∈ R_l(n)\) where \(m\) is within the range as well.
This means that nodes \(m\) and \(n\) restrict a range on the y-axis that can be seen highlighted in blue (Fig. 5).
</p>

<p>
As we have traversed through all of the dimensions once (there are only two steps when considering a two-dimensional plane),
we can conclude that the remaining nodes' regions must be contained within the query rectangle (Fig. 6).
This means that in this case we can add the whole subtree, with the root being that node \(n\), without having to make any further intersection checks on the subtree.
Please note that there might be multiple nodes left after the last step of the algorithm; we need to add the subtree of all of these, because all these regions are contained.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.2.-Pseudocode"><h4 id="Pseudocode">Pseudocode</h4></div>

<pre>
	Input:
		A node
	Output:
		The axis parent node for the input node, if it exists. Otherwise return null
	function GetAxisParentNode(node)
		parentNode = node.parent
		
		while parentNode != null &amp;&amp; parentNode.axis != node.axis
			parentNode = parentNode.parent	
		end while
		
		return parentNode
	end function
	
	Input:
		A node
		A range query of d dimensions
		A number representing the dimensionality of space
	Output:
		Boolean value if node is contained within range
	function IsContainedWithin(node, min_max, dimensionality)
		
		focus_node := node
		
		// Keeps track of the final region bounds
		min_max2 := []
		
		for index from 0 to dimensionality
			// Move onto the next parent node
			focus_node = parent(focus_node)
			
			// Root node reached; cannot be fully contained
			if focus_node == null
				return false
			end if
			
			axis := axis(focus_node)
			
			axis_parent := GetAxisParentNode(focus_node)
			
			if axis_parent == null
				return false
			end if
			
			// Get the minimum and maximum for this axis
			min := min(focus_node.value[axis], axis_parent.value[axis])
			max := max(focus_node.value[axis], axis_parent.value[axis])
			
			// Check whether the node is bound this range
			if not min &lt;= focus_node.value[axis] &lt;= max
				return false
			end if
			
			min_max2.push([min, max])
		end for
		
		// Finally, check that the containment min/max are inside the range query.
		for dimension from 0 to dimensionality
			min1 := min(min_max[dimension])
			max1 := max(min_max[dimension])
			min2 := min(min_max2[dimension])
			max2 := min(min_max2[dimension])
			
			// If the range min_max isn't fully contained in min_max2 on dimension d, then it is not fully contained
			return false if not (min1 &lt;= min2 &lt;= max1) or (min1 &lt;= max2 &lt;= max1)
		end for
		
		// Otherwise, the region is fully contained.
		return true
	end function
</pre>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.3."><h3 id="5.3.">5.3.</h3></div>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.3.-Base runtime"><h4 id="Base runtime">Base runtime</h4></div>

<p>
As usual, range checking and leaf checking are \(Θ(1)\) operations.
We will ignore the containment check for now.
</p>

<p>
The algorithm traverses the tree linearly, meaning it will take the same amount of time for each layer of the tree. Similarly, this is true for reporting the subtrees, as each layer will be only covered once.
</p>

<p>
Checking for intersection is a \(Θ(d)\) operation for \(d\) dimensions, but because most often \(d \ll n\), in order to simplify this recursion we can say it is \(Θ(1)\).
</p>

<p>
The complexity of traversing the tree can be solved using a recursion. In the worst case, node \(v\) intersects both \(R_l(v)\) and \(R_r(v)\), and we have to search both sides.
</p>

<p>
This means our recurrence \(T(n) = 2*T(n/2)+Θ(1)\).
Using the master method we can easily find that it fulfills case 1. This means \(T(n) = Θ(n^{\log_2{2}}) = Θ(n)\).
</p>

<p>
For \(n\) elements, we would have to perform \(n\) steps for each element, so the complexity becomes \(Θ(n^2)\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.3.-Runtime with optimizations"><h4 id="Runtime with optimizations">Runtime with optimizations</h4></div>

<p>
Having the additional "is fully contained within" check reduces the amount of recursion on large trees drastically.
The algorithm only has to check the regions which intersect the range query, spending no additional time on the inside regions of the query rectangle.
</p>

<p>
The complexity of <code>IsFullyContainedWithin</code> itself is only \(Θ(d)\), as all the other checks are constant time operations. In cases where \(d \ll n\), we can look at <code>IsFullyContainedWithin</code> as a \(Θ(1)\) operation.
</p>

<p>
Again, note that the tree is traversed linearly: traveling all the layers of the tree takes \(Θ(k)\) time for a query of \(k\) items returned.
As we are only considering the intersection steps, we are essentially looking for the recursion that is inherent from a range search in the tree.
We can think of the range search as follows: in a rectangle, we are looking at four distinct lines that cross a certain number of regions.
For four rectangle's sides, as \(4*Θ(x) = Θ(x)\) where \(Θ(x)\) is the complexity of traveling a single line, we can look at a single line and figure out how many regions are crossed.
(For higher dimensions, this number is \(d^2*Θ(x)\), but unless \(x \gg d^2\) for a large \(n\), it does not matter (note that \(n \ll d\)), we can ignore the dimension here).
</p>

<p>
Looking more closely at one line, this line can either cross the left region or the right region. However, in the next axes it will always cross double the amount of regions it crossed in the first axis, as these regions lie perpendicular to the line.
More precisely, if the axis of the node does not match the axis of the plane, it will always cross \(2^{d-1}\) regions of that node.
</p>

<p>
This gives us a recurrence, applying to all the nodes that do not match the axis of the plane. The recursion step (<code>IsFullyContainedWithin</code>) is \(Θ(1)\) for simplicity's sake.
\(T(n) = 2*(T(n/4)) + Θ(1)\)
</p>

<p>
We can once again solve this recurrence using the master method.
Case 1 holds, which means the recurrence solves to \(T(n) = Θ(n^{\log_4{2}}) = Θ(n^{0.5}) = Θ(\sqrt{n})\).
</p>

<p>
The combined runtime of the previous steps is thus \(Θ(\sqrt{n}) + Θ(k) = Θ(\sqrt{n}+k)\), meaning that the KD-tree can be queried in \(Θ(\sqrt{n}+k)\) time where \(n\) is the amount of items and \(k\) is the amount of results returned by the query.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.4."><h3 id="5.4.">5.4.</h3></div>

<p>
A KD-tree does not intuitively care about a point's numerical coordinate. The coordinate is only used to determine the order in which the space is partitioned and the nodes are traversed after tree construction. If we can find a deterministic way to order two points with the same coordinate in an axis and traverse the resulting KD-tree later, we can use exactly the same process as we did before.
</p>

<p>
We can achieve this result by exploiting the fact that for any two points \(a = (x_0, x_1, ..., x_d)\) and \(b = (x_0, x_1, ..., x_d)\) in \(d\) dimensions will have \(i ≥ 1\) distinct coordinate points in \(d-i\) distinct coordinate points. (If all the coordinate points would match in each dimension, the points would be the same)
</p>

<p>
In other words, for points \(a\) and \(b\), instead of defining non-unique coordinates as real numbers, we can define composite number coordinates of form \(c_0 = (x_0 | x_1 | ... | x_d)\) that are truly unique for each point, allowing us to order each point along one axis regardless of similar coordinates.
</p>

<p>
For example, a two-dimensional composite number coordinate can be ordered using lexicographic ordering:
\((a | b) &lt; (a' | b')\) iff \(a &lt; a' ∨ (a = a' ∧ b &lt; b')\).
</p>

<p>
We notice that the algorithms defined in earlier stages of this report only need a truly ordered list of elements to work with, which the composite numbers do a very good job in ensuring. Now that we can have a lexicographic order over single values in an axis, the algorithms work just as well as with real numbers.
</p>

<p>
When querying, a composite number can then be converted into a real number (a point coordinate) by looking at the first element of the composite number. This means that we can afterwards just do the conversion and end up with the correct result.
</p>

<p>
See <code>Task54.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.5."><h3 id="5.5.">5.5.</h3></div>

<p>
Searching for a circular / spherical / hyper-spherical region can be simplified to answering a simple query: is the Euclidean distance for a location of a given point \(v\) and the center of our region \(w\) less than the radius \(r\) of our (hyper)-sphere?
</p>

<p>
Formally:
</p>

<p>
Set our region to be a tuple of form \((w,r)\), where \(w\) is the center point of the (hyper)-sphere, and the radius is \(r ≥ 0\).
We can then calculate the Euclidean distance \(v_{dist}\) in \(d\) dimensions between the two points \(v\) and \(w\), using the Pythagorean formula \(\sqrt{ (v_1-w_1)^2 + \dots + (v_d-w_d)^2 }\).
If the distance \(v_{dist} ≤ r\), this means the point lies inside the query region. This can be used for the leaf checking, or more specifically checking whether a point is inside a region \((w,r)\) in \(d\) dimensions.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.5.-Intersection check"><h4 id="Intersection check">Intersection check</h4></div>

<p>
Checking whether a region intersects can be done easily as well. Given a root node \(v\), there are two possibilities for the position of the position of \(w\). Either \(v &lt; w\) (\(w\) lies on the right side), or \(v ≥ w\) (\(w\) lies on the left side).
Let us look more closely at the former situation. If \(w\) lies on the right side of \(v\), this means it will definitely intersect region \(R_r\). However, we need to check whether the sphere intersects \(R_l\) as well, in case the radius \(r\) is large enough to intersect the left side as well. Mathematically, this means that \(r ≥ (w - x_d)\) for any dimension \(d\) and the corresponding coordinate point \(x_d ∈ v\).
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.5.-Intersection check-Pseudocode"><h5 id="Pseudocode">Pseudocode</h5></div>

<pre>

Input:
	A point in n-dimensional space
	A point in n-dimensional space
Output:
	The euclidean distance between these two points
function distance(point1, point2)
	sum := 0
	for each dimension in point1 and point2
		delta := point1[dimension]-point2[dimension]
		add delta to sum
	end for
	return square root of sum
end function

Input:
	A node
	The position of the circle
	The radius of the circle
Output:
	A boolean whether the circle intersects the region of this node
function intersect_left(node, position, radius)
	// Intersection check for the left region
	axis := node.axis
	value := node.value[axis]
	
	if position[axis] &lt;= value
		// Position is less than the node, so the circle must intersect the left region
		return true
	else
		// Position is more than the node, but the circle might still intersect via its radius
		return position[axis]-radius &lt;= value
	end if
end function

Input:
	Same as intersect_left
Output:
	Same as intersect_left
function intersect_right(node, position, radius)
	The same code as in intersect_left, signs and the mentions 'right' and 'left' swapped around.
end function

</pre>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.5.-Containment check"><h4 id="Containment check">Containment check</h4></div>

<p>
Observe that the minimum circular range query that can contain a region \(R(v)\) must include all of the corner points of this region. (Fig 11.)
</p>

<p>
Further, the maximum distance \(u_{max}\) between any corner point of the region and the query's center point \(w\) dictates the smallest radius \(r\) for the circle query where the given region is fully contained.
</p>

<p>
This means that, given we know \(u_{max}\), if \(r ≤ u_{max}\) the region \(R\) is indeed fully contained. Again, as Euclidean distance works in any number of dimensions and all of the points in the (hyper)-sphere are as distance \(r\) from \(w\), the containment check works for any number dimensions.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.5.-Containment check-Pseudocode"><h5 id="Pseudocode">Pseudocode</h5></div>

<pre>

Input:
	A list of ranges for each dimension, of form ((x_min,x_max),(y_min,y_max),...), number of ranges depending on the number of dimensions.
Output:
	A list of points that are the corners of the min_max query rectangle.
function GetCorners(min_max)
	dimensionality := length(min_max)
	
	corners := []
	// For a rectangle in d dimensions, there are 2^d corners.
	for every c in 2^d
		this_corner := []
		for i from 0 to dimensionality
			// Loop through every combination of elements in min_max, choosing 0 0 0, 0 0 1, 0 1 0, 0 1 1, etc...
			this_corner.push(min_max[c][i/(2^c) % 2])
		end for
		corners.push(this_corner)
	end for
	
	return corners
end function

Input:
	A node.
	The position of the range (hyper)-sphere.
	The radius of the range (hyper)-sphere.
Output:
	A boolean whether the node's region is fully contained within the (hyper)-sphere.
function IsFullyContainedWithin(node, position, radius)
	region := region of node
	
	if the region contains infinite bounds on any axis
		return false
	end if
	
	// Calculate all the possible combinations of the min_max region. This gives you the corners of the region.
	corners := GetCorners(region)
	
	// Get the maximum distance to any corner
	max_dist := get the maximum distance(position, corner) for all of the corners
	
	// If the max_dist &lt;= radius, the region is fully contained inside the (hyper)-sphere
	contained := max_dist &lt;= radius
	
	return contained
end function

</pre>

<p>
Please see <code>Task55.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.5.-Comparison with a regular range query"><h4 id="Comparison with a regular range query">Comparison with a regular range query</h4></div>

<p>
The difference to a regular n-dimensional range query is that the range is the same for all dimensions. This removes the need for a slightly more complex intersection query, but does not affect the whole algorithm's theta runtime, as after the intersection checks the regular recursion is still in place.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.6."><h3 id="5.6.">5.6.</h3></div>

<p>
A convex polygonal region can be used as a range query, when additional precision is needed in querying the set of points.
This is done by modifying the leaf check to check whether a point is inside a convex hull - this is a \(Θ(e)\) operation for \(e\) edges (and \(v = e\) nodes) in the convex hull.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.6.-Intersection check"><h4 id="Intersection check">Intersection check</h4></div>

<p>
The intersection check can be done in a few different ways.
An optimized approach for convex polygons is to just check whether any corner of the region is inside the convex polygon (See Fig. 21).
Checking whether a point lies inside a (convex) polygon can be done in \(Θ(c)\) time where \(c\) is the number of points in the convex query.
</p>

<p>
As we are handling a strictly convex polygon, we can use the bounding box of this polygon to act as a heuristic search range in the intersection check.
As the regions are rectangular, we are not checking too many unnecessary regions, and as the shapes are convex, the area that is 'lost' that is outside of the polygon but inside the bounding box is small.
This generally works well, is efficient (\(Θ(1)\)) to compute, but has a few edge cases that are good to be considered in achieving the best runtime.
For example, in Fig. 19, you can see a case where the intersection check using the bounding box approach returns a false positive, meaning that while the bounding box does intersect these regions, the convex hull does not.
If the queries are similar to this case, this method might not achieve the best runtime.
</p>

<p>
Another way of checking the intersection is slightly more complex, but will produce more predictable results, and reduce the checks done in a dense tree dramatically.
This method involves looping through the edges of the convex shape (\(Θ(e)\)) and checking for intersections with the given region edges.
If there is an intersection between those lines, then the convex shape intersects the region. Another pro of this method is that it will work for all kinds of polygons, not just convex ones.
While this method is more accurate, there is a tradeoff especially when dealing with large convex shapes, as the algorithm still is \(Θ(e)\) where \(e\) is the number of edges in the polygon, for each node visited.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.6.-Containment check"><h4 id="Containment check">Containment check</h4></div>

<p>
In order for the region to be fully contained inside the convex region, all of the four points of the region need to be inside the convex polygon. Intersection checks are not needed as the polygon is convex; a straight line can be drawn from any point inside the area to another without intersections between the borders.
</p>

<p>
Please see <code>Task56.rb</code> for implementation.
</p>

<div id="CS2521 Algorithmic Problem Solving-Section 5-5.6.-Comparison with a regular range query"><h4 id="Comparison with a regular range query">Comparison with a regular range query</h4></div>

<p>
The difference between the convex polygonal region to the range searching methods introduced in earlier exercises is that a convex region requires either the same amount or more intersection checks for each layer in the tree, depending on the intersection implementation.
This can have a significant impact on the performance depending on the type of KD-tree searched; for trees that are evenly spaced out and queries that are relatively complex polygons, the bounding box method will work the most efficiently. However, for trees with high density regions, the second implementation will work better, as it will reduce the amount of unnecessary checks performed on the KD-tree leaves that happen to fall on the 'lost' area in the bounding box.
</p>

<div id="CS2521 Algorithmic Problem Solving-Figures"><h2 id="Figures">Figures</h2></div>

<p>
<img src="graph1.png" />
</p>

<p>
Fig. 1: The average time taken to construct data structures described in tasks 1.1, 1.2, and 2.1, in seconds. The error bars represent standard deviation between repeats. Please note the use of a logarithmic scale.
</p>

<p>
<img src="graph2.png" />
</p>

<p>
Fig. 2: The average time taken to query the same data structures as in Fig. 1. The error bars represent standard deviation between repeats.
</p>

<p>
<img src="fig1.png" />
</p>

<p>
Fig. 3: An example 2-dimensional KD-tree, with the current node in consideration visible, and the minimum / maximum values (\(r_s\),\(r_f\)) of the range query visible, with the range query being colored green.
</p>

<p>
<img src="fig4.png" />
</p>

<p>
Fig. 4: A node in a KD-tree which is fully contained inside the range query. Note that the highlighted node \(v\) has two regions, \(R_l\) and \(R_r\) for left and right regions respectively. When talking about a node's region, we usually mean both \(R_l\) and \(R_r\).
</p>

<p>
<img src="fig3.png" />
</p>

<p>
Fig. 5: The first step in determining whether a region is fully contained in a 2-dimensional range query. Highlighted on the bottom is the node \(w\) where \(r_s ≤ w ≤ r_f\), and on the top we have the original node \(v\). The blue highlighted area is the region that is contained between the range on dimension \(d_{l+1}\) (y-axis in this case).
</p>

<p>
<img src="fig2.png" />
</p>

<p>
Fig. 6: The second step in determining whether a region is fully contained in a 2-dimensional range query. Highlighted on the left is node \(v\), and on the right it's axis parent node \(v_{ap}\). Please see Fig. 7. and section 5.2. for an explanation of the axis parent node.
</p>

<p>
<img src="axisparent1.png" />
</p>

<p>
Fig. 7: A node \(v\) and its axis parent \(v_{ap}\). Note that the nodes share the same axis.
</p>

<p>
<img src="axisparent2.png" />
</p>

<p>
Fig. 8: Another node \(v\) and the corresponding axis parent \(v_{ap}\). Note how the axis is always the same.
</p>

<p>
<img src="55int1.png" />
</p>

<p>
Fig. 9: Situation where the node's left region is affected (\(v ≥ w\)) and the right one is not (\(r &lt; (w - x_d)\)).
</p>

<p>
<img src="55int2.png" />
</p>

<p>
Fig. 10: Situation where both the left and right regions are affected (\(r ≥ (w - x_d)\)).
</p>

<p>
<img src="55fig1.png" />
</p>

<p>
Fig. 11: A situation where a region is fully contained; all of the corner regions are included inside the circle.
</p>

<p>
<img src="55fig2.png" />
</p>

<p>
Fig. 12: A situation where a region is not fully contained but yet intersected; some of the corner points fall outside of the query circle, depicted in grey.
</p>

<p>
<img src="55fig3.png" />
</p>

<p>
Fig. 13: Minimum radius for a fully containing query is the distance from the circle position to the furthest corner point.
</p>

<p>
<img src="55fig4.png" />
</p>

<p>
Fig. 14: Radius checking against the minimum range and the actual value, where \(u_{max} ≤ r\).
</p>

<p>
<img src="56bb1.png" />
</p>

<p>
Fig. 18: Bounding box: good / average case scenario. There are no false positives. Regions that intersect are colored light red.
</p>

<p>
<img src="56bb2.png" />
</p>

<p>
Fig. 19: Bounding box: bad case scenario - regions that are true positives are still light red, and false positives have been colored dark red.
</p>

<p>
<img src="56fig1.png" />
</p>

<p>
Fig. 20: A case where a convex query fully contains the region. All the nodes are inside the convex shape and there are no intersections. Notice how the intersection step is not really needed; for a convex shape it is sufficient to merely check whether all the corners of the region are inside the range query.
</p>

<p>
<img src="56fig2.png" />
</p>

<p>
Fig. 21: A case where a convex query does not fully contain the region. One of the corner points is outside of the query, and additionally there are two intersections in the edges of the region.
</p>

<p>
<img src="56fig3.png" />
</p>

<p>
Fig. 22: A case where a convex query does not fully contain the region, but does not intersect with the region edges.
</p>
